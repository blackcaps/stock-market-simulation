<!DOCTYPE html>
<html lang="en">
    <head>
        <title>"Portfolio"</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="resources/css/bootstrap.min.css" type="text/css"/>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
         <script src="resources/js/jquery-1.11.3.min.js"></script>
        
         <script src="resources/js/bootstrap.min.js"></script>
       
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
        <script src="resources/js/Chart.bundle.js"></script>
        
        <link rel="stylesheet" type="text/css" href="resources/css/sidebarcss.css"/>
        <link rel="stylesheet" type="text/css" href="resources/css/portfoliocss.css"/>
        <link rel="stylesheet" type="text/css" href="resources/css/rightClockBar.css"/>
        <link rel="stylesheet" type="text/css" href="resources/css/playerhidetxtbox.css"/>
        
   <link rel="stylesheet" type="text/css" href="resources/css/clock.css"/>
           
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2" >
                   <div class="sidenav">
                        <div class="user-data">
                        <img class="user-dp" src="resources/images/team-2.jpg" alt="img"> 
                        <span class="username" id="uname">Name</span>
                        </div>
                       
                       <a href="#">Portfolio</a>
                       <a href="#" onclick='return trade()'>Trade</a>
                       <a href="#" onclick='return broker()'>Broker</a>
                        <a href="#" onlick='return scores()'>Scores</a>
                        
                   </div>
                </div>
                <div class="col-md-8" style="background-color: white">
                    <div class="heading">
                    <h3>Portfolio</h3> 
                     <hr>
                    <div class="input-group input-group-lg">
                        <h5>Player Name: <input id="input_player_name" type="text" class="form-control"></h5>
                    </div>
                   <div class="input-group input-group-lg">
                        <h5>Account Value: <input id="input_account_value" type="text" class="form-control"></h5>
                    </div>
                    <h5 id="select-a-stock">Select a Stock name to view more details</h5>
                    </div>
                    <div class="table-area">
                        <table id="table" class="table">
	                        <tr>
	                            <th>Name</th>
	                            <th>Bought price</th>
	                            <th>Current Price</th>
	                        </tr>
	                    <!--     <tr>
	                            <td>ABC</td>
	                            <td>30</td>
	                            <td>50</td>
	                        </tr>
	                        <tr>
	                            <td>EAD</td>
	                            <td>20</td>
	                            <td>60</td>
	                        </tr>
	                        <tr>
	                            <td>DEC</td>
	                            <td>60</td>
	                            <td>70</td>
	                        </tr> -->
                    </table>
                    </div>
                   
                    <div class="canvas-format">
                        <canvas id="scoreChart" width="400"></canvas>
                    </div>
                </div>
                <div class="col-md-2 col-sm-2">
                     <div class="colright">
                      <div class="form-style-5">
                    <form>
                        <span>
                        <label for="trans">Remaining Time </label>
                        </span>
                        <div id="ten-countdown"  class="div" style="display: block;"></div>
                        <!-- <script type="text/javascript" src="resources/js/clock.js"></script> -->
                        <hr>
                        <div class="turn">
                            <h5><b>Turn Number</b></h5>
                        <input id="input_turn_no" type="text" name="field1" id="Tno">
                        <hr>
                        </div>
                        <p id="txtone"></p>
                        <input type="text" id="tbone" name="field1">
                        <br>
                         <p id="txttwo"></p>
                        <input type="text" id="tbtwo" name="field1">
                        <br>
                        <p id="txtthree"></p>
                        <input type="text" id="tbthree" name="field1">
                        <br>
                        <p id="txtfour"></p>
                        <input type="text" id="tbfour" name="field1">
                        <br>
                        <p id="txtfive"></p>
                        <input type="text" id="tbfive" name="field1">
                        <br>
                        <p id="txtsix"></p>
                        <input type="text" id="tbsix" name="field1">
                    </form>
                      
                </div>
                     </div>
                </div>
            </div>  
       </div>
         <script src="resources/js/linechart-with_table.js">
             
        </script>
        <script src="resources/js/playervisibility.js">
            
        </script>
        
        <script type="text/javascript">
        	$(document).ready(function() {  
                var username = document.getElementById("uname");
                username.innerHTML = getCookie("player");

                var xmlHttp = new XMLHttpRequest();
                var reqpath = "/playerdetail/" + getCookie("id");
                xmlHttp.open( "GET", reqpath, false ); // false for synchronous request
                xmlHttp.send( null );
                var objj = JSON.parse(xmlHttp.response);

                document.getElementById("input_player_name").value = objj["playerName"];
                document.getElementById("input_account_value").value = objj["amount"];
                
       		    setInterval(function(){ 
       		    	//alert("Hello"); 
       		    	var xhttp = new XMLHttpRequest();
    			    xhttp.open("GET", "/remaining_time", false);
    			    xhttp.send();
    			    var response = xhttp.responseText;
    			    if (isNaN(response)) {
    			    	document.getElementById("ten-countdown").innerHTML = response;
    			    } else {
	    			    var minutes = Math.floor(response / 60);
	    			    var seconds = response - minutes * 60;	    			    
	    			    document.getElementById("ten-countdown").innerHTML = minutes + " min " + seconds + " sec";
                    }
                    xhttp.open("GET", "/get_turn", false);
                    xhttp.send();
                    var turn_no = xhttp.responseText;
                    document.getElementById("input_turn_no").value = turn_no;

       		    }, 1000);  

                setupTabel();
                addRowHandlers();
		
        	});
        	
        	function trade() {
        		window.location.replace("/trade");
            };
            
            function broker() {
                window.location.replace("/broker");
            };

            function scores() {
                window.location.replace("/scores");
            }

            function setupTabel() {
                table = document.getElementById("table");
                var xmlHttp = new XMLHttpRequest();
                xmlHttp.open( "GET", "/sampletest", false ); // false for synchronous request
                xmlHttp.send( null );
                var obj = JSON.parse(xmlHttp.response);

                 for(var i=0;i<obj.length;i++) {
                    var tr="<tr>";
                    var td1="<td>"+obj[i]["name"]+"</td>";
                    var td2="<td>"+Number(obj[i]["boughtPrice"]).toFixed(2)+"</td>";
                    var td3="<td>"+Number(obj[i]["currentPrice"]).toFixed(2)+"</td></tr>";

                    $("table").append(tr+td1+td2+td3); 
                    // $(table).append(row$);
                }

                alert(JSON.stringify(obj));
                // for(var i=0;i<obj.length;i++) {
                //     var row$ = $('<tr/>');
                //     for (var colIndex = 0; colIndex < 3; colIndex++) {
	            //         var cellValue = "hola";
	            //         row$.append($('<td/>').html(cellValue));
                //     }
                //     $(table).append(row$);
                // }
            };
            
            function addRowHandlers() {
         		var table = document.getElementById("table");
         		var rows = table.getElementsByTagName("tr");
         		for (i = 0; i < rows.length; i++) {
         	    	var currentRow = table.rows[i];
         	  		var createClickHandler = function(row) {
         	      		return function() {
         	        		var cell = row.getElementsByTagName("td")[0];
         					var id = cell.innerHTML;
         					alert("id:" + id);
         				};
         			};
         			currentRow.onclick = createClickHandler(currentRow);
         		}	
            };

            function getCookie(cname) {
                var name = cname + "=";
                var ca = document.cookie.split(';');
                for(var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') {
                        c = c.substring(1);
                    }
                    if (c.indexOf(name) == 0) {
                        return c.substring(name.length, c.length);
                    }
                }
                return "";
            };
        	
        	function populate_table() {
                alert('clckd');
        		var jarray = '[{"player":{"id":101,"name":"arsha"},"balance":1002.45},{"player":{"id":103,"name":"sartar"},"balance":973.0}]';
                
        		myobj = JSON.parse(jarray);
        		// var txt;
        		// for(x in jarray) {
        		// 	txt += myobj[x].name;
        		// }
                alert('clicked');
        		document.getElementById("select-a-stock").innerHTML = myobj[0].player.id + " " + myobj[1].player.name;
        	};
        </script>
    </body>
</html>
