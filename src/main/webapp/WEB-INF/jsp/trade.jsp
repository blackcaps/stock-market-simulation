<!DOCTYPE html>
<html>
    <head>
        <title>
            trade
        </title>
        
       
        <link rel="stylesheet" type="text/css" href="resources/css/sidebarcss.css">
        <!-- <script type="text/javascript" src="resources/js/trade_drop_box_pass_value.js"></script> -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        
        <link rel="stylesheet" href="resources/css/bootstrap.min.css" type="text/css"/>
        <link rel="stylesheet" type="text/css" media="all" href="resources/css/trade.css">
        <link rel="stylesheet" type="text/css" href="resources/css/rightClockBar.css"/>
        <link rel="stylesheet" type="text/css" href="resources/css/playerhidetxtbox.css"/>
        <link rel="stylesheet" type="text/css" href="resources/css/clock.css"/>
    </head>
    <body>
       
        
  <div class="row">
    <div class="column left">
               
                <div class="sidenav">
  <div class="user-data">
     <img class="user-dp" src="resources/images/team-2.jpg" alt="img"> 
     <span class="username" id="uname">Player Name</span>
  </div>
  <a href="#" onclick="return portfolio()">Portfolio</a>
  <a href="#">Trade</a>
  <a href="#" onclick="return broker()">Broker</a>
  <a href="#" onclick="return bank()">Bank</a>
  <a href="#" onclick="return scores()">Scores</a>
</div>
            </div>
            <div class="column middle">
                <div>
                    <form class="form-style-5">
                        <fieldset>
                            <label for="sector">Sector</label>
                            <select id="sector" name="field1" onChange="changecat(this.value); getSelectValue();" >
                                <option value="" disabled selected>Select</option>
                                <option value="1">FINANCE</option>
                                <option value="2">CONSUMER</option>
                                <option value="3">TECHNOLOGY</option>
                                <option value="4">MANUFACTURING</option>

                            </select> 
                            
                            <label for="stock">Stock</label>
                            <select id="stock" name="stock">

                                <option value="" disabled selected>Select</option>

                            </select>
                            <hr>
                            <label for="trans">Transaction</label>
                            <select id="trans" name="field1">

                                <option value="buy">Buy</option>
                                <option value="sell">Sell</option>

                            </select>

                            <label>Quantity</label>
                            <input id="quantity-input" type="text" onkeydown="return numOnly(event);" />
                            <hr>
                            <button id="submit-button" class="button" style="vertical-align:middle"
                                    onclick="buyStock()"><span>Submit Order</span>
                            </button>
                        </fieldset>
                    </form>
                    <br>
                    <br>
                    <br>
                    <br>
                             <div class="canvas-format">
                        <canvas id="scoreChart" width="400"></canvas>
                    </div>
                </div>
                
            </div>
            <div class="colright">
                      <div class="form-style-5">
                    <form>
                        
                        <label for="trans">Remaining Time </label>
                        <div id="ten-countdown"  class="div" style="display: block;"></div>
                        <!-- <script type="text/javascript" src="resources/js/clock.js"></script> -->
                        <hr>
                        <div class="turn">
                            <h5><b>Turn Number</b></h5>
                        <input type="text" name="field1" id="Tno">
                        <hr>
                        </div>
                        <p id="txtone"></p>
                        <input type="text" id="tbone" name="field1">
                        <br>
                         <p id="txttwo"></p>
                        <input type="text" id="tbtwo" name="field1">
                        <br>
                        <p id="txtthree"></p>
                        <input type="text" id="tbthree" name="field1">
                        <br>
                        <p id="txtfour"></p>
                        <input type="text" id="tbfour" name="field1">
                        <br>
                        <p id="txtfive"></p>
                        <input type="text" id="tbfive" name="field1">
                        <br>
                        <p id="txtsix"></p>
                        <input type="text" id="tbsix" name="field1">
                    </form>
                      
                </div>
                     </div>
        </div>
        <script src="resources/js/linechart.js">
            
        </script>
         <script src="resources/js/playervisibility.js">
            
        </script>

        <script type="text/javascript">
            $(document).ready(function() {              
                setInterval(function(){ 
                    //alert("Hello"); 
                    var xhttp = new XMLHttpRequest();
                    xhttp.open("GET", "/remaining_time", false);
                    xhttp.send();
                    var response = xhttp.responseText;
                    if (isNaN(response)) {
                        document.getElementById("ten-countdown").innerHTML = response;
                    } else {
                        var minutes = Math.floor(response / 60);
                        var seconds = response - minutes * 60;                      
                        document.getElementById("ten-countdown").innerHTML = minutes + " min " + seconds + " sec";
                    }
                }, 1000);   

                // selectoptions();            
            });   

            // function selectoptions() {
            //     selector = document.getElementById("sector");
            //     selector.options.add(new Option('Keyrslkfj','Valueeeuigsj'));
            // }

            var stocksBySectors = {
                1: ["HSBC", "LOLC", "SAMPATH", "NSBC"],
                2: ["BELLTACO", "MCLOVIN", "KINGBURGER", "HUTPIZZA"],
                3: ["98Y", "MEGAHARD", "LABCAKES", "KELLSJOHN"],
                4: ["SONY", "VONOLE", "GATEWAY", "HP"]
            };

            function changecat(value) {
                if (value.length === 0)
                    document.getElementById("stock").innerHTML = "<option></option>";
                else {
                    var catOptions = "";
                    for (categoryId in stocksBySectors[value]) {
                        catOptions += "<option>" + stocksBySectors[value][categoryId] + "</option>";
                    }                    
                    document.getElementById("stock").innerHTML = catOptions;
                }
            };

            function alphaOnly(event) {
  			    var key = event.keyCode;
  			    return ((key >= 65 && key <= 90) || key == 8 || (key >= 37 && key <=40) || key == 46);
		    };

            function numOnly(event) {
                var key = event.keyCode;
                return ((key >= 48 && key <= 57) || (key >= 96 && key <= 105) || key == 8 || (key >= 37 && key <=40) || key == 46);
            };

            function transaction() {
                var json_str = {"firstName":"John", "lastName":"Doe"}
            };

            function portfolio() {
                window.location.replace("/portfolio");
            };

            function broker() {
                window.location.replace("/broker")
            };

            function bank() {
                window.location.replace("/bank");
            };

            function scores() {
                window.location.replace("/scores");
            };

            function buyStock() {
                var sector = document.getElementById("sector");
                var sector_value = sector.options[sector.selectedIndex].text;
                var stock = document.getElementById("stock");
                var stock_value = stock.options[stock.selectedIndex].value;
                var transaction = document.getElementById("trans");
                var transaction_value = transaction.options[transaction.selectedIndex].text;
                var quantity = document.getElementById("quantity-input");
                var quantity_value = quantity.value;
                  
                if (quantity.value === "" || quantity.value === null || quantity.value == 0) {
                    alert("Please mention a valid quantity!");
                    return;
                }
                var j_str = {"sector": sector_value, "stock": stock_value, "transaction": transaction_value, "quantity": quantity_value};
                // alert(json_str);

                var xmlHttp = new XMLHttpRequest();
    	        xmlHttp.open( "POST", "/addtransaction", false ); // false for synchronous request
                xmlHttp.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
    	        xmlHttp.send(JSON.stringify(j_str));

                var response = xmlHttp.responseText;
                // if(response == "success")
                alert(response);
                // else
                    // alert("failed");
            };
                 
        </script>
    </body>
</html>