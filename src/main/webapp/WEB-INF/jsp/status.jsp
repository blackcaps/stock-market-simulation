<!DOCTYPE html>
<html lang="en">
    <head>
        <title>"Status"</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css"/>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
         <script src="js/jquery-1.11.3.min.js"></script>
        
         <script src="js/bootstrap.min.js"></script>
       
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
        <script src="js/Chart.bundle.js"></script>

        <link rel="stylesheet" type="text/css" href="css/sidebarcss.css"/>
        
        <link rel="stylesheet" type="text/css" href="css/rightClockBar.css"/>
  
    <link rel="stylesheet" type="text/css" href="css/rightClockBar.css"/>
        <link rel="stylesheet" type="text/css" href="css/statuscss.css"/>
          <link rel="stylesheet" type="text/css" href="css/playerhidetxtbox.css"/>
            <link rel="stylesheet" type="text/css" href="css/clock.css"/>
       
    </head>
    <body>
        <div class="container">
            <div class=" row">
                <div class="col-md-2">
                     <div class="sidenav">
                        <div class="user-data">
                        <img class="user-dp" src="images/team-2.jpg" alt="img"> 
                        <span class="username" id="uname">Player Name</span>
                        </div>
                       
                       <a href="Portfolio.html">Portfolio</a>
                       <a href="trade.html">Trade</a>
                       <a href="Brocker.html">Broker</a>
                       <a href="Bank.html">Bank</a>
                        <a href="scores.html">Scores</a>
                        
                   </div>
                </div>
                
            
                <div class="col-md-7">
                    <div id="statusname">
                        <h1>Status</h1>
                    </div> 
                    <div class="selector">
                    
                        <select id="namelist" onchange="getSelectValue();"> 
                            <option value="" disabled selected>Select Player</option>
                            <option value="A">Events</option>
                            <option value="B">Stock Prices</option>
                         </select>
                    
                    </div>
                    <div class="wrapper">
                    
                        <div class="statustable">
                            <table id="table" class =table>
                                
                            </table>
                        
                            <div id ="jj" class="canvas-format">
                            <canvas id="scoreChart"  width="100"></canvas>
                        </div>
                    </div>
              </div>
                </div>     
                
                <div class="col-md-3">
                    <div class="colright">
                      <div class="form-style-5">
                    <form>
                        
                        <label for="trans">Remaining Time </label>
                        <div id="ten-countdown"  class="div"></div>
                        <script type="text/javascript" src="js/clock.js"></script>
                        <hr>
                        <div class="turn">
                            <h5><b>Turn Number</b></h5>
                        <input type="text" name="field1" id="Tno">
                        <hr>
                        </div>
                        <p id="txtone"></p>
                        <input type="text" id="tbone" name="field1">
                        <br>
                         <p id="txttwo"></p>
                        <input type="text" id="tbtwo" name="field1">
                        <br>
                        <p id="txtthree"></p>
                        <input type="text" id="tbthree" name="field1">
                        <br>
                        <p id="txtfour"></p>
                        <input type="text" id="tbfour" name="field1">
                        <br>
                        <p id="txtfive"></p>
                        <input type="text" id="tbfive" name="field1">
                        <br>
                        <p id="txtsix"></p>
                        <input type="text" id="tbsix" name="field1">
                    </form>
                          
                          
                </div>
                     
                </div>
                    
                </div>
            </div>
     
        </div>
          <script src="js/statusjs.js"></script>
          <script src="js/statushidetable.js"></script>
       </body>
       
     </html>