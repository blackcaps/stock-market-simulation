<!DOCTYPE html>
<html lang="en">
    <head>
        <title>"Bank"</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="resources/css/bootstrap.min.css" type="text/css"/>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
         <script src="resources/js/jquery-1.11.3.min.js"></script>
        
         <script src="resources/js/bootstrap.min.js"></script>
       
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
        <script src="resources/js/Chart.bundle.js"></script>
 
        <link rel="stylesheet" type="text/css" href="css/sidebarcss.css"/>
        
        <link rel="stylesheet" type="text/css" href="resources/css/rightClockBar.css"/>
        <link rel="stylesheet" type="text/css" href="resources/css/playerhidetxtbox.css"/>
        <link rel="stylesheet" type="text/css" href="resources/css/clock.css"/>
        <link rel="stylesheet" type="text/css" href="resources/css/bank.css"/>
           
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                 <div class="sidenav">
                        <div class="user-data">
                        <img class="user-dp" src="resources/images/team-2.jpg" alt="img"> 
                        <span class="username" id="uname">Player Name</span>
                        </div>
                       
                       <a href="#">Portfolio</a>
                       <a href="#">Trade</a>
                       <a href="#">Broker</a>
                       <a href="#">Bank</a>
                        <a href="#">Scores</a>
                        
                   </div>
                </div>
               
                 <div class="col-md-6">
                    
                     <div class="brockermiddle">
                         <h1>Bank Details</h1>
                         <br><br>
                            <div class="wrapper">
                           
                                <h4>Player Name:<label></label></h4>
                            <br><br>
                            <h4>Transaction History:</h4>
                            
                           <table id="table" class="table">
                        <tr>
                            <th>Transaction Details</th>
                            <th>Credit(Income)</th>
                            <th>Debit Expense</th>
                        </tr>
                        <tr>
                            <td>ABC</td>
                            <td>30</td>
                            <td>50</td>
                        </tr>
                        <tr>
                            <td>EAD</td>
                            <td>20</td>
                            <td>60</td>
                        </tr>
                        <tr>
                            <td>DEC</td>
                            <td>60</td>
                            <td>70</td>
                        </tr>
                    </table>
                  <h4>Account Balance : <label></label></h4>
                 </div>
                </div>
                 </div>
       
                 
        <div class="col-md-3">
                     <div class="colright">
                      <div class="form-style-5">
                    <form>
                        
                        <label for="trans">Remaining Time </label>
                        <div id="ten-countdown"  class="div"></div>
                        <script type="text/javascript" src="resources/js/clock.js"></script>
                        <hr>
                        <div class="turn">
                            <h5><b>Turn Number</b></h5>
                        <input type="text" name="field1" id="Tno">
                        <hr>
                        </div>
                        <p id="txtone"></p>
                        <input type="text" id="tbone" name="field1">
                        <br>
                         <p id="txttwo"></p>
                        <input type="text" id="tbtwo" name="field1">
                        <br>
                        <p id="txtthree"></p>
                        <input type="text" id="tbthree" name="field1">
                        <br>
                        <p id="txtfour"></p>
                        <input type="text" id="tbfour" name="field1">
                        <br>
                        <p id="txtfive"></p>
                        <input type="text" id="tbfive" name="field1">
                        <br>
                        <p id="txtsix"></p>
                        <input type="text" id="tbsix" name="field1">
                    </form>
                          
                          
                </div>
                     
                </div>
                
            </div>
            </div>
        </div>
        <script src="resources/js/playervisibility.js">
            
        </script>
    </body>
</html>