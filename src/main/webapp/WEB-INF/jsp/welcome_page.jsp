<!DOCTYPE html>
<html lang="en">
    <head>
        <title>"Welcome Page"</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="resources/css/bootstrap.min.css" type="text/css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="resources/js/jquery-1.11.3.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="resources/css/style.css"/>
        <link rel="stylesheet" href="css/animate.min.css"/>
        <script src="js/bootstrap.js"></script>
    </head>
    <body>
        <!--first row-->
        <div class ="container-fluid banner">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                       <div id="slider" class="carousel slide">
         
                        <ol class="carousel-indicators">
                        <li data-target="#slider" data-slide-to="0" class="active"></li>
                         <li data-target="#slider" data-slide-to="1"></li>
                        <li data-target="#slider" data-slide-to="2"></li>
                        </ol>
                         
                           <div class="carousel-inner">
                                    <div class="item active">
                                        <img id="ol" src="resources/images/grr.jpg" alt="image" class="img-responsive">
                                            <div class="carousel-caption">
                                                <h1 id="main">Team BLACK-CAPS</h1>
                                                 <p>Our Stock Market Simulator is developed for giving you a 
                                                    clear definitive idea and a practical knowledge about how to invest in Stock Market.
                                                </p>
                                             </div>
                                    </div><!--closing item active class-->
                                    <div class="item">
                                        <img src="resources/images/ddd.jpg" alt="image" class="img-responsive">
                                            <div class="carousel-caption">
                                                <h2>Go through steps</h2>
                                                <p>Black Caps Stock Market Simulator will guide you to become 
                                                   the investor you dreamed to be.Go through the steps.Game will guide you theorotically
                                                   and practically with fun.
                                                </p>
                                            </div>
                                    </div><!--closing active-->
                
                                    <div class="item">
                                        <img src="resources/images/pg.jpg" alt="image" class="img-responsive">
                                            <div class="carousel-caption">
                                            <h2>Be professional</h2>
                                             <p>
                                                Our app is not just a knowledge hub for stock market,You will  learn big part of stock market 
                                                just by playing a game .
                                             </p>
                                            </div>
                                    </div><!--closing active-->
                            </div><!--closing carausel-inner-->
                        </div><!--closing carausel-slide-->
                      <a class="carousel-control left" href="#slider" data-slide="prev"><span class="icon-prev"></span></a>
                      <a class="carousel-control right" href="#slider" data-slide="next"><span class="icon-next"></span></a>
                   </div><!--closing slide-->
                </div><!--closing row-->
        </div><!--closing container--> 
        <!--2nd roww-->
        <div id ="PlayBar" class ="playbar">
            <div class="container wow fadeInUp" data-wow-delay="1.0s">
                <!-- <div class="row"> -->
                	<div id="start-game-collapsible-button" class="row collapse">
                		<button class="btn btn-primary" style="padding-left: 100px; padding-right: 100px" onclick=
                			"httpGet()"><h4>Click to start the game!</h4></button>
                	</div>
                	<div class="row">
	                	<div class="collapse col-lg-4" id="content">
	                    	<div class="well">
	                        	<div class="input-group input-group-lg">
	                            	<h5>Player Name: <input id="input-create-player" type="text" class="form-control" onkeydown="return alphaOnly(event);"></h5>
	                             	<button id="button-create-game" class="btn btn-primary pull-left" style="margin-top: 10px;"
	                             		data-toggle="collapse" data-target="#start-game-collapsible-button" onclick="createGame()">Create Game</button>
	                             	<label id="label-create-game" style="margin-top: 15px;"></label>
	                        	</div>
	                    	</div>
	                    </div>
	                   <!--  <div class="col-lg-12" style="margin: 0 auto; text-align=center;">
                        	<h1 style="text-align=center;"><b>CREATE OR JOIN THE GAME</b></h1>
                    	</div> -->
	                    <div class="collapse col-lg-4 float-right pull-right" id="contenttwo">	                    	
                        	<div class="well">
                            	<div class="input-group input-group-lg">
                                	<h5>Player Name: <input id="input-join-game" type="text" class="form-control" onkeydown="return alphaOnly(event);"></h5>
                                 	<button id="button-join-game" class="btn btn-primary pull-left" style="margin-top: 10px;">Join Game</button>
                                 	<label id="label-join-game" style="margin-top: 15px; margin-left: 5px"></label>
                            	</div>
                        	</div>                        	
                    	</div>
	                </div>
                    <div class="col-lg-4 col-md-4"><i class="glyphicon glyphicon-edit"></i>
                        <br>
                        <button id="button-create-game-main" style="margin: 10px;" class="btn btn-primary" data-toggle="collapse" data-target="#content"
                        	onclick="toBottom()">Create Game</button>
                        
                   	</div>
                   
                    <div class="col-lg-4 col-md-4 pull-right">
                    	
                        <i class="glyphicon glyphicon-eye-open"></i>
                        <br>
                        <button id="button-join-game-main" style="margin: 10px;" class="btn btn-primary" data-toggle="collapse" data-target="#contenttwo" 
                        	onclick="toBottom()">Join Game</button>
                        
                	</div>
                	<div class="col-lg-12">
                        	<h1 style="text-align:center;"><b>CREATE OR JOIN THE GAME</b></h1>
                    </div>
             	<!-- </div> -->
          	</div>
       </div>
   <script src="resources/js/wow.min.js"></script>
    <script>
              new WOW().init();
    </script>
    <script type="text/javascript">
    	function toBottom()
		{
			window.scrollTo(0, document.body.scrollHeight);	
		};

        function alphaOnly(event) {
  			var key = event.keyCode;
  			return ((key >= 65 && key <= 90) || key == 8 || (key >= 37 && key <=40) || key == 46);
		};
    	
    	function httpGet()
    	{
            var xmlHttp = new XMLHttpRequest();
            var playerName = document.getElementById("input-create-player").value;

            // alert(playerName);
            var path = "createplayer/"
            path = path + playerName;
    	    xmlHttp.open( "GET", path, false ); // false for synchronous request
    	    xmlHttp.send( null );
    	    // alert("player created!");
    	    //window.location = xmlHttp.responseText;
    	    window.location.replace("/portfolio");
        };

        function joinGame() {
            var xmlHttp = new XMLHttpRequest();
            var playerName = document.getElementById("input-join-game").value;

            var path = "createplayer/"
            path = path + playerName;
    	    xmlHttp.open( "GET", path, false ); // false for synchronous request
    	    xmlHttp.send( null );
    	   
    	    window.location.replace("/portfolio");
        }
        
        function createGame() {
            var btn = document.getElementById("input-create-player");
            if (btn.value == null || btn.value == "" || btn.value == 0) {
                alert("Please enter a valid name!");
                return;
            }

            var xmlHttp = new XMLHttpRequest();
            var path = "creategame/";
    	    xmlHttp.open( "GET", path, false ); // false for synchronous request
    	    xmlHttp.send( null );
            alert("Game created!");
            
            var startgame = $('#start-game-collapsible-button');
            $(startgame).collapse('show');
            $(btncreategame).attr('disabled', 'disabled');
            $('#button-create-game-main').attr('disabled', 'disabled');
            $('#button-join-game-main').attr('disabled', 'disabled');
            $('#button-join-game').attr('disabled', 'disabled');
        }

        function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for(var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        };

		$(document).ready(function() {           

            // var btncreategame = $('#button-create-game');
               			
            // var startgame = $('#start-game-collapsible-button');
            // // alert('whats wrong');
            // $(btncreategame).click(function() {
            //     $(startgame).collapse('show');
            //     $(btncreategame).attr('disabled', 'disabled');
            //     $('#button-create-game-main').attr('disabled', 'disabled');
            //     $('#button-join-game-main').attr('disabled', 'disabled');
            //     $('#button-join-game').attr('disabled', 'disabled');
            // });            

            // var gameid = getCookie("gameid");
            // alert("Welcome to the game " + gameid);
		});
		
			
    </script>

    </body>
</html>
