function countdown(elementName, minutes, seconds)
{
    var element, endTime, hours, mins, msLeft, time;

    function twoDigits(n)
    {
        return (n <= 9 ? "0" + n : n);
    }

    function updateTimer()
    {
        msLeft = endTime - (+new Date);
        if (msLeft < 1000) {
            element.innerHTML = "Time is up!";
        } else {
            time = new Date(msLeft);
            hours = time.getUTCHours();
            mins = time.getUTCMinutes();
            element.innerHTML = (hours ? hours + ':' + twoDigits(mins) : mins) + ':' + twoDigits(time.getUTCSeconds());
            setTimeout(updateTimer, time.getUTCMilliseconds() + 500);
        }
    }

    element = document.getElementById(elementName);
    endTime = (+new Date) + 1000 * (60 * minutes + seconds) + 500;
    updateTimer();
}

countdown("ten-countdown", 10, 0);

function passvalue()
{
    var mealsByCategory = {
        A: ["Soup", "Juice", "Tea", ],
        B: ["Soup", "Juice", "Water", "Others"],
        C: ["Soup", "Juice", "Coffee", "Tea", "Others"]
    }

    function changecat(value) {
        if (value.length === 0)
            document.getElementById("stock").innerHTML = "<option></option>";
        else {
            var catOptions = "";
            for (categoryId in mealsByCategory[value]) {
                catOptions += "<option>" + mealsByCategory[value][categoryId] + "</option>";
            }
            document.getElementById("stock").innerHTML = catOptions;
        }
    }
}