package com.blackcaps.model;

public class BankAssistant {
	private String playerName;
	private double amount;
	
	public BankAssistant() {
		
	}
	
	public BankAssistant(String playerName, double amount) {
		this.playerName = playerName;
		this.amount = amount;
	}

	public String getPlayerName() {
		return playerName;
	}
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
}
