package com.blackcaps.model;

public class Broker {
	int id;
	Player player;
	Transaction transaction;
	Sector sector;
	Stock stock;
	int transactionType;
	double unitPrice;
	int quantity;
	
	public Broker() {
		
	}

	public Broker(int id, Player player, Transaction transaction, Sector sector, Stock stock, int transactionType,
			double unitPrice, int quantity) {
		this.id = id;
		this.player = player;
		this.transaction = transaction;
		this.sector = sector;
		this.stock = stock;
		this.transactionType = transactionType;
		this.unitPrice = unitPrice;
		this.quantity = quantity;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Player getPlayer() {
		return player;
	}
	
	public void setPlayer(Player player) {
		this.player = player;
	}
	
	public Transaction getTransaction() {
		return transaction;
	}
	
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}
	
	public Sector getSector() {
		return sector;
	}
	
	public void setSector(Sector sector) {
		this.sector = sector;
	}
	
	public Stock getStock() {
		return stock;
	}
	
	public void setStock(Stock stock) {
		this.stock = stock;
	}
	
	public int getTransactionType() {
		return transactionType;
	}
	
	public void setTransactionType(int transactionType) {
		this.transactionType = transactionType;
	}
	
	public double getUnitPrice() {
		return unitPrice;
	}
	
	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}	
}
