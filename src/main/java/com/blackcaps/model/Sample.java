package com.blackcaps.model;

public class Sample {
	private String name;
	private double boughtPrice;
	private double currentPrice;
	
	public Sample() {

	}
	
	public Sample(String name, double boughtPrice, double currentPrice) {
		super();
		this.name = name;
		this.boughtPrice = boughtPrice;
		this.currentPrice = currentPrice;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getBoughtPrice() {
		return boughtPrice;
	}
	public void setBoughtPrice(double boughtPrice) {
		this.boughtPrice = boughtPrice;
	}
	public double getCurrentPrice() {
		return currentPrice;
	}
	public void setCurrentPrice(double currentPrice) {
		this.currentPrice = currentPrice;
	}	
}
