package com.blackcaps.model;

public class TransactionAssistant {
	private int playerId;
	private int transactionType;
	private double amount;
	private String comment;
	private int sectorId;
	private int stockId;
	private int quantity;
	
	public TransactionAssistant() {
		
	}

	public TransactionAssistant(int playerId, int transactionType, double amount, String comment, int sectorId, int stockId, int quantity) {
		this.playerId = playerId;
		this.transactionType = transactionType;
		this.amount = amount;
		this.comment = comment;
		this.sectorId = sectorId;
		this.stockId = stockId;
		this.quantity = quantity;
	}

	public int getPlayerId() {
		return playerId;
	}
	
	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}
	
	public int getTransactionType() {
		return transactionType;
	}
	
	public void setTransactionType(int transactionType) {
		this.transactionType = transactionType;
	}
	
	public double getAmount() {
		return amount;
	}
	
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	public String getComment() {
		return comment;
	}
	
	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getSectorId() {
		return sectorId;
	}

	public void setSectorId(int sectorId) {
		this.sectorId = sectorId;
	}

	public int getStockId() {
		return stockId;
	}

	public void setStockId(int stockId) {
		this.stockId = stockId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
