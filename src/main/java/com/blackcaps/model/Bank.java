package com.blackcaps.model;

public class Bank {
	Player player;
	double balance;
	
	public Bank() {
		
	}
	
	public Bank(Player player, double balance) {
		this.player = player;
		this.balance = balance;
	}

	public Player getPlayer() {
		return player;
	}
	
	public void setPlayer(Player player) {
		this.player = player;
	}
	
	public double getBalance() {
		return balance;
	}
	
	public void setBalance(double balance) {
		this.balance = balance;
	}
}
