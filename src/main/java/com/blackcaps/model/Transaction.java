package com.blackcaps.model;

public class Transaction {
	private int transactionId;
	private Player player;
	private int transactionType;
	private double transactionAmount;
	private String comment;
	private int turn;
	
	public Transaction() {
		
	}

	public Transaction(int transactionId, Player player, int transactionType, double transactionAmount,
			String comment, int turn) {
		this.transactionId = transactionId;
		this.player = player;
		this.transactionType = transactionType;
		this.transactionAmount = transactionAmount;
		this.comment = comment;
		this.turn = turn;
	}

	public int getTransactionId() {
		return transactionId;
	}
	
	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}
	
	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}
	
	public int getTransactionType() {
		return transactionType;
	}
	
	public void setTransactionType(int transactionType) {
		this.transactionType = transactionType;
	}
	
	public double getTransactionAmount() {
		return transactionAmount;
	}
	public void setTransactionAmount(double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getTurn() {
		return turn;
	}

	public void setTurn(int turn) {
		this.turn = turn;
	}
	
}
