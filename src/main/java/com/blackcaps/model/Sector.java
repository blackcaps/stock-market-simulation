package com.blackcaps.model;

import java.util.List;

public class Sector {
	private int id;
	private String name;
	private List<Stock> stocks;
	
	public Sector() {
		
	}

	public Sector(int id, String name, List<Stock> stocks) {
		this.id = id;
		this.name = name;
		this.stocks = stocks;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public List<Stock> getStocks() {
		return stocks;
	}
	
	public void setStocks(List<Stock> stocks) {
		this.stocks = stocks;
	}
	
}
