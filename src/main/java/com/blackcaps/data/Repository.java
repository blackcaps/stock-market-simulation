package com.blackcaps.data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.blackcaps.constant.Stocks;
import com.blackcaps.model.Broker;
import com.blackcaps.model.Player;
import com.blackcaps.model.Sector;
import com.blackcaps.model.Stock;
import com.blackcaps.model.Transaction;

public class Repository {
	
	private static Map<Integer, Transaction> bankTransactions = new HashMap<>();
	private static Map<Integer, Double> bank = new HashMap<>();
	private static Map<Integer, Broker> broker = new HashMap<>();
	private static Map<Integer, Player> players = new HashMap<>();
	
	private static Map<Integer, Sector> sectors = new HashMap<>();
	private static Map<Integer, Stock> stocks = new HashMap<>();
	
	private static Map<Integer, Map<Integer, Integer>> stockQuantityForPlayers = new HashMap<>();
	
	private static Map<Integer, List<Double>> stockPriceHistory = new HashMap<>();
	private static Map<Integer, String> eventStreamNames = new HashMap<>();
	
	private static double[][] marketPrices;
	
	public static Map<Integer, Transaction> getBankTransactions() {
		return bankTransactions;
	}

	public static Map<Integer, Double> getBank() {
		return bank;
	}
	
	public static Map<Integer, Broker> getBroker() {
		return broker;
	}

	public static Map<Integer, Player> getPlayers() {
		return players;
	}

	
	public static Map<Integer, Sector> getSectors() {
		return sectors;
	}

	public static Map<Integer, Stock> getStocks() {
		return stocks;
	}
	
	public static Map<Integer, List<Double>> getStockPriceHistory(){
		return stockPriceHistory;
	}

	public static double[][] getMarketPrices() {
		return marketPrices;
	}

	public static void setMarketPrices(double[][] marketPrices) {
		Repository.marketPrices = marketPrices;
	}

	public static Map<Integer, String> getEventStreamNames() {
		return eventStreamNames;
	}

	public static void setEventStreamNames(Map<Integer, String> eventStream) {
		Repository.eventStreamNames = eventStream;
	}

	public static Map<Integer, Map<Integer, Integer>> getStockQuantityForPlayers() {
		return stockQuantityForPlayers;
	}
}
