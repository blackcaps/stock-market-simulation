package com.blackcaps.data;

import java.util.HashMap;
import java.util.Map;

import com.blackcaps.model.Sector;
import com.blackcaps.model.Stock;

public class MarketRepository {

	private static Map<Integer, Sector> sectors = new HashMap<>();
	private static Map<Integer, Stock> stocks = new HashMap<>();
	
	public static Map<Integer, Sector> getSectors() {
		return sectors;
	}
	
	public static Map<Integer, Stock> getStocks() {
		return stocks;
	}
	
}
