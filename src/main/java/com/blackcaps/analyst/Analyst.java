package com.blackcaps.analyst;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;

import com.blackcaps.data.Repository;
import com.blackcaps.stock.service.NewTimerService;
import com.blackcaps.stock.service.StockService;

public class Analyst {
	
	@Autowired
	StockService stockService;

	public synchronized static int buyExpertOpinion(double[][] prices, int turn) {
		double[][] marketPrices = prices;
		
		if (turn > 10) {
			return 0;
		}
		
		Map<Integer, Double> values = new HashMap<>();
		
		for (int i = 0; i < 16; i++) {
			double initialVal = marketPrices[turn - 1][i];
			double maxVal = initialVal;
			
			for (int j = turn; j < 10; j++) {
				if (marketPrices[j][i] > maxVal) {
					maxVal = marketPrices[j][i];
				}
			}
			values.put(i, maxVal - initialVal);
		}
		
//		System.out.println(values);
//		System.out.println();
		
		Map<Integer, Double> sortedVal = desSort(values);
//		System.out.println(sortedVal);
//		System.out.println();
		
		List<Integer> keys = new ArrayList<>(sortedVal.keySet());
//		System.out.println(keys.get(0));
//		System.out.println(keys.get(1));
//		System.out.println(keys.get(2));
		
		return keys.get(0);
		
	}
	
	//send original stock ids
	public static synchronized void sellExpertOpinion(double[][] prices, int[] stockIds, int turn) {
		if (turn > 10) {
			return;
		}
		
		int[] stocks = stockIds;
		//Stock ID's start from one, but arrays start from 0
		//so array[0] refers to the first stock
		for (int stock : stocks) {
			stock = stock - 1;
		}
		
		Map<Integer, Double> values = new HashMap<>();
		
		for (int i = 0; i < 16; i++) {
			double initialVal = prices[turn - 1][i];
			double maxVal = 0;
			
			if (arrayContains(stocks, i)) {
				for (int j = turn; j < 10; j++) {
					if (prices[j][i] > maxVal)
						maxVal = prices[j][i];
				}
				//if the resulting value is negative, the price will never go up than the current price
				values.put(i, maxVal - initialVal);
			}
		}
		
		Map<Integer, Double> sortedVal = ascSort(values);
//		System.out.println(values);
//		System.out.println();
//		
//		System.out.println(sortedVal);
//		System.out.println();
	}
	
	
	
	
	public static boolean arrayContains(int[] array, int value) {
		for (int i = 0; i < array.length; i++) {
			if (array[i] == value)
				return true;
		}
		return false;
	}
	
	
	
	
	public static Map<Integer, Double> desSort(Map<Integer, Double> unsortMap) {

	    List<Entry<Integer, Double>> list = new LinkedList<Entry<Integer, Double>>(unsortMap.entrySet());
	
	    Collections.sort(list, new DesComparator());
	
	    Map<Integer, Double> sortedMap = new LinkedHashMap<Integer, Double>();
	    for (Entry<Integer, Double> entry : list) {
	    	sortedMap.put(entry.getKey(), entry.getValue());
	    }
	    return sortedMap;
	}
	
	public static Map<Integer, Double> ascSort(Map<Integer, Double> unsortMap) {

	    List<Entry<Integer, Double>> list = new LinkedList<Entry<Integer, Double>>(unsortMap.entrySet());
	
	    Collections.sort(list, new AscComparator());
	
	    Map<Integer, Double> sortedMap = new LinkedHashMap<Integer, Double>();
	    for (Entry<Integer, Double> entry : list) {
	    	sortedMap.put(entry.getKey(), entry.getValue());
	    }
	    return sortedMap;
	}
	
	public static int getRecommendation() {
		if (new Random().nextInt(10) < 6) {
			int stockId = buyExpertOpinion(Repository.getMarketPrices(), NewTimerService.getCurrentTurn());
			return stockId + 1;
		}
		else {
			return new Random().nextInt(16);
		}
	}
}


class DesComparator implements Comparator<Entry<Integer, Double>> {
	public int compare(Entry<Integer, Double> o1, Entry<Integer, Double> o2) {
		return o2.getValue().compareTo(o1.getValue());
	}
}

class AscComparator implements Comparator<Entry<Integer, Double>> {
	public int compare(Entry<Integer, Double> o1, Entry<Integer, Double> o2) {
		return o1.getValue().compareTo(o2.getValue());
	}
}
