/**
 * 
 */
package com.blackcaps.old;
import java.util.Random;

import com.blackcaps.market_algo.CalPrices;



/**
 * @author Sadeepa
 *
 */
public final class VariationProbability_old {
	
	private static double price = 1000;
	private static int nvoes=0,nxtval=0;
	private static Random trendcal = new Random();
	public static double[][] prices = new double[10][16];
	//############################################################################################
	//							all trends in array index = turn
	//############################################################################################
	
	static int[] random_trend = new int[10];							
	static int[][] sector_trend = new int[10][4];
	static int[][] market_trend = new int[10][16];
			
	//############################################################################################
	//										All Trend Getters
	//############################################################################################
	public static void getRandomTrend() {
		for(int l=0;l<=9;l++)
		{
			System.out.print(random_trend[l]+" , ");
		}
		System.out.println();
	}
	
	//GET all the sectors trends or for each turn or for each turn with desired sector
	public static void getSector_T()
	{
		for(int i =0;i<10;i++)
		{
			for(int j =0;j<4;j++)
			{
				System.out.print("Sector Trend "+sector_trend[i][j]+" , "+i+" ");
			}
			System.out.println();
		}
	}
	
	public static void getSector_T(int turn)
	{
		System.out.println(sector_trend[turn][0]);
		System.out.println(sector_trend[turn][1]);
		System.out.println(sector_trend[turn][2]);
		System.out.println(sector_trend[turn][3]);
	}
	
	public static int getSector_T(int turn,int j)
	{
		return sector_trend[turn][j];
	}
	
	//GET Market trends below for each turn or each turn with the desired market
	public static int getMarket_T(int turn,int j)
	{
		return market_trend[turn][j];
	}
	
	public static int getMarket_T(int turn)
	{
		for(int o = 0;o<15;o++)
		{
			System.out.println(market_trend[turn][o]);
		}
		return 0;
	}
	
	public static void getMarket_T()
	{
		for(int i =0;i<16;i++)
		{
			for(int j =0;j<10;j++)
			{
				System.out.print("Market Trend "+market_trend[j][i]+" , "+j+" ");
			}
			System.out.println();
		}
	}
	//##########################################################################################
	//above getTrend method have to delete after testing
	//##########################################################################################
		
	//*******************************************************************************************
	
	//						IMPORTANT neverOverEstimate() method highly used for trend setters
	
	//*******************************************************************************************
	private static int neverOverEstimate(int max,int trendindex,int difference,int currentValue)	//never over or underestimate market difference
	{
		nvoes = (int)trendcal.nextInt(100);
		nxtval = currentValue;
		if(trendindex == 0)
		{
			if(nxtval < max && nvoes<25)			//.25 probability of market decrease
			{
				return nxtval=nxtval+difference; 
			}
			else if(nxtval > 0 && nvoes<50)		//.25 probability of market increase
			{
				return nxtval=nxtval-difference;
			}else if(nvoes<100)
			{return nxtval;}					//.5  probability of market not changing
		}
		else if(trendindex == 1)
		{
			if(nxtval < max && nvoes<25)			//.25 probability of market decrease
			{
				return nxtval=nxtval+difference; 
			}
			else if(nxtval > 0 && nvoes<50)		//.25 probability of market increase
			{
				return nxtval=nxtval-difference; 
			}else if(nvoes<100)
			{return nxtval;}					//.5  probability of market not changing
		}
		else if(trendindex == 2)
		{
			if(nxtval < max && nvoes<25)			//.25 probability of market decrease
			{
				return nxtval=nxtval+difference; 
			}
			else if(nxtval > 0 && nvoes<50)		//.25 probability of market increase
			{
				return nxtval=nxtval-difference; 
			}else if(nvoes<100)
			{return nxtval;}					//.5  probability of market not changing
		}
		return nxtval;
	}
	//##########################################################################################################
	//Assume 1st 4 are 1st sectors, and 2nd 4 are the second sectors, so on;
	//##########################################################################################################
	public static void priceset()
	{
		for(int k=0;k<16;k++)
		{
			prices[0][k] = CalPrices.calprice(market_trend[0][0],sector_trend[0][0],random_trend[0],price);
		}
		
		for(int j=0;j<16;j++)
		{
			for(int i=1;i<10;i++)
			{	// price = ;
				if(j<4)
					prices[i][j] = CalPrices.calprice(market_trend[i-1][j],sector_trend[i][0],random_trend[i-1],price);
				// price = 500;
				if(j<8)
					prices[i][j] = CalPrices.calprice(market_trend[i-1][j],sector_trend[i][1],random_trend[i-1],price);
				if(j<12)
					prices[i][j] = CalPrices.calprice(market_trend[i-1][j],sector_trend[i][2],random_trend[i-1],price);
				if(j<16)
					prices[i][j] = CalPrices.calprice(market_trend[i-1][j],sector_trend[i][3],random_trend[i-1],price);

			}
		}
	}
	//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	//							New Trend Setters unique for each sector,market, :)
	//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	//all the trends in, are generated between -3 to 3 and -2 to 2, as 6 to 0 and 4 to 0
	// example = -2 as 0 , -1 as 1 , 0 as 2 , 1 as 3 , 2 as 4
	//the pre computed trend then assign to the int array 
	//all the old way of setting the trends are lie below
	//														THANK YOU!!!
	//##########################################################################################
		
	public static void setRandomTrend()
	{	
		random_trend[0] = trendcal.nextInt(4);		//random trend set
		for(int w =0;w<9;w++)
		{
			random_trend[w+1] = neverOverEstimate(4,2,1,random_trend[w]);
		}
	}
	public static void setmarketTrend()
	{
		{
			for(int i =0;i<16;i++)
			{
				market_trend[0][i] = trendcal.nextInt(6);
			}
		}
		for(int o =1;o<10;o++)
		{
			for(int i =0;i<=15;i++)
			{
				market_trend[o][i] = neverOverEstimate(6,0,1,market_trend[o-1][i]);
			}
		}
	}
	public static void setSector_Trend()
	{
		for(int i =0;i<4;i++)
		{
			sector_trend[0][i] = trendcal.nextInt(6);
		}
		for(int o =0;o<4;o++)
		{
			for(int i =1;i<10;i++)
			{
				sector_trend[i][o] = neverOverEstimate(6,0,1,sector_trend[i-1][o]);
			}
		}
	}

	//############################################################################################
	// Additional Methods to get each elemental detail to sectors and market
	//############################################################################################
	public static double[][] getPrices()
	{
		return prices;
	}
	
	public static void showAllprices(int turn)
	{
		for(int j = 0; j<16;j++)
			{System.out.println("From showAll "+turn+" "+j+" "+prices[turn][j]);}
	}
	
	public static void showSectorPrices(int turn,String sector)
	{
		if(sector.equals("Finance"))
		{
			for(int i = 0;i<4;i++)
			{
				System.out.println(sector +" "+prices[turn][i]);
			}
		}
		if(sector.equals("Consumer"))
		{
			for(int i = 4;i<8;i++)
			{
				System.out.println(sector +" "+prices[turn][i]);
			}
		}
		if(sector.equals("Technology"))
		{
			for(int i = 8;i<12;i++)
			{
				System.out.println(sector +" "+prices[turn][i]);
			}
		}
		if(sector.equals("Technology"))
		{
			for(int i = 12;i<16;i++)
			{
				System.out.println(sector +" "+prices[turn][i]);
			}
		}
	}
	public static void showMarketPrices(int turn,String[] market)
	{
		
	}
}	
