package com.blackcaps.old;

import java.util.Random;

public final class EventSet_old {
	
	static private String[][] event_probability = new String[5][2]; // made a 2d array where u save event in 1st column probability in the 2nd one
	static private int random_event = 0;
	static private Random event_set = new Random();
	static int lftover;				//dummy value for leftover array elements to increase after each turn u will understand when looking at the code ;)
	private static int count =0;
	private static int posibility = 0;
	
	private static void setEvents()
	{
		for(int i = 0;i < 5;i++)
		{
			switch(i){
			case 0:event_probability[i][0] = "Crash";break;
			case 1:event_probability[i][0] = "Boom";break;
			case 2:event_probability[i][0] = "Bust";break;
			case 3:event_probability[i][0] = "TakeOver";break;
			case 4:event_probability[i][0] = "Scandal";break;}
		}
	}
	
	private static void setEventProbability(int turn)
	{
		random_event = event_set.nextInt(5); 
		// basic idea is to input the probability dynamically in the 2nd column of the array and get back the which was occurred 
		//and which did not and from that get the remaining events to occur benefit is can add .1 probability as well after each turn
		//###################### big change in code the values change with after each turn events are called in a random order 
		//random mapping for all the 10 turns can change to any number of turns
		if(turn == 1)
		{
			for(int i = 0;i < 5;i++)
			{
				event_probability[i][1] = "20";
			}
			event_probability[random_event][1] = "0";			// do all this for random event to be 0
		}
		else if(turn == 2)
		{
			lftover = 0;
			random_event = event_set.nextInt(5);
			for(int i = 0;i<5;i++)
			{
				if(event_probability[i][1].equals("0"))
				{
					lftover = Integer.parseInt(event_probability[i][1]);
					lftover = lftover + 10;
					event_probability[i][1] = Integer.toString(lftover);
					lftover = 0;
				}else if(event_probability[i][1] != "0")
				{
					lftover = Integer.parseInt(event_probability[i][1]);
					lftover = lftover + 5;
					event_probability[i][1] = Integer.toString(lftover);
					lftover = 0;
				}
			}
			event_probability[random_event][1] = "0";
		}
		else if(turn == 3)
		{
			lftover = 0;
			random_event = event_set.nextInt(5);
			for(int i = 0;i<5;i++)
			{
				if(event_probability[i][1].equals("0") || event_probability[i][1].equals("10"))
				{
					lftover = Integer.parseInt(event_probability[i][1]);
					lftover = lftover + 10;
					event_probability[i][1] = Integer.toString(lftover);
					lftover = 0;
				}else if(event_probability[i][1] != "0")
				{
					lftover = Integer.parseInt(event_probability[i][1]);
					lftover = lftover + 5;
					event_probability[i][1] = Integer.toString(lftover);
					lftover = 0;
				}
			}
			event_probability[random_event][1] = "0";
		}
		else if(turn == 4)
		{
			lftover = 0;
			random_event = event_set.nextInt(5);
			for(int i = 0;i<5;i++)
			{
				if(event_probability[i][1].equals("0")|| event_probability[i][1].equals("10")|| event_probability[i][1].equals("20"))
				{
					lftover = Integer.parseInt(event_probability[i][1]);
					lftover = lftover + 10;
					event_probability[i][1] = Integer.toString(lftover);
					lftover = 0;
				}else if(event_probability[i][1] != "0")
				{
					lftover = Integer.parseInt(event_probability[i][1]);
					lftover = lftover + 5;
					event_probability[i][1] = Integer.toString(lftover);
					lftover = 0;
				}
			}
			event_probability[random_event][1] = "0";
		}
		else if(turn == 5)
		{
			lftover = 0;
			random_event = event_set.nextInt(5);
			for(int i = 0;i<5;i++)
			{
				if(event_probability[i][1].equals("0")|| event_probability[i][1].equals("10")|| event_probability[i][1].equals("20")|| event_probability[i][1].equals("30"))
				{
					lftover = Integer.parseInt(event_probability[i][1]);
					lftover = lftover + 10;
					event_probability[i][1] = Integer.toString(lftover);
					lftover = 0;
				}else if(event_probability[i][1] != "0")
				{
					lftover = Integer.parseInt(event_probability[i][1]);
					lftover = lftover + 5;
					event_probability[i][1] = Integer.toString(lftover);
					lftover = 0;
				}
			}
			event_probability[random_event][1] = "0";
		}
				// ################################################### have to remove below for loop ########################################	
		for(int j = 0;j<5;j++)
		{
			System.out.println(event_probability[j][0]+" "+event_probability[j][1]);
		}
				//################################################# remove above for loop ##################################################
	}
	
	public static double eventCall(int turn,double price)
	{	// random number between 100 and 1 where is the 67 possibility lie in the highest way 1 to 67 has more than 68 to 100 :)
		if(count  == 0)
		{
			setEvents();
			count = count + 1;
		}
		setEventProbability(turn);
		if(event_set.nextInt(10)<=6)	// tricky i know but this probability always have to be less than 68 for this to work
		{
			price = setEventPrices(turn,price);
			
		}
		
		return price;
	}
	
	private static double setEventPrices(int turn,double price)
	{	
		random_event = event_set.nextInt(5); 
		posibility  = event_set.nextInt(10);
		double checkprob = Integer.parseInt(event_probability[random_event][1]);
		String check = event_probability[random_event][0]; // because now this random event is the one executing so now possibility of it is 0
		event_probability[random_event][1]="0";		 // but setting the possibility of the event to 0 after picking it
		
		if(turn >= 2 || turn <= 5)
		{
			if(posibility >= 5)
			{	if(check.equals("Boom") && checkprob == 0)
				{
					price=price + event_set.nextInt((5 - 1) + 1) + 1;					// price change with a Boom
				}
				else if(check.equals("Bust") && checkprob == 0)
				{
					price=price -  event_set.nextInt(((-1) - (-5)) + 1) + (-5);			// price change with a Bust
				}
			}
		}
		if(turn >= 1 || turn <=7)
		{
			if(posibility >= 7)
			{
				if(check.equals("TakeOver") && checkprob == 0)
				{
					price= price - event_set.nextInt(((-1) - (-5)) + 1) + (-5);			// price change with a TakeOver
				}
			}
			else if(posibility > 5)
			{
				if(check.equals("ProfitWarning") && checkprob == 0)		
				{
					price= price + event_set.nextInt((3 - 1) + 2) + 2;					// price change with a crash
				}
			}
			else
			{
				if(check.equals("Scandal") && checkprob == 0)
				{
					price=price - event_set.nextInt(((-3) - (-6)) + 2) + (-6);			// price change with a Scandal
				}
			}					
		}
		return price;
	}
}
