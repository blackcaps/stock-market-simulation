package com.blackcaps.market_algo;


public final class Anaylst {
	// for the gamers to feel like instructions are not coming just from a code :) 
	private static String a =null,ban=null,countOn=null,advice=null;
	private static int arrayIndex = -1,marketIndex=-1,c=0;
	private static String avoid= null;	
	//###########################################################################################
	//				Advice Giver
	//###########################################################################################
	public static String getRecommandation(int turn,String Sector,String Market){
	if(turn != 1)
	{
		
		a = null;
		//#######################################################################################
		//Indexing each sector to a number to check price list 
		//example 3 to below all array elements are finance sectors
		// 4 to 7 are all consumer sectors so start from 7 and check below all
		//#######################################################################################
				
		if(checkSector(turn) == true)
		{
			a = Sector+" Sector trends are Increasing you should Buy Stocks to Increase your profit, Sir";
		}else
		{
			a = Sector+" Sector trends are Decreasing you should Sell Stocks to Lesser your losses, Sir";
		}
		return a + "/n"+ checkMarket(turn);
	}
	else
	{
		a = "Please play more than one turn for analyst to predict!! Good Luck! :)";
		return a;
	}
	
	}
	//#############################################################################################
	//					Sector Check method for the analyst to check which to invest
	//#############################################################################################

	public static boolean checkSector(int turn)
	{
		for(int i =0;i<=3;i++)
		{
			if(VariationProbability.getSector_T(turn,i)>3)
			{
				c++;
				
			}else{
				c--;
			}
		}
		if(c<0)
		{
			c=0;
			return false;
		}
		else
		{
			c=0;
			return true;
		}
	}
	//#############################################################################################
	//					Market Check method for the analyst to check which to invest
	//#############################################################################################
	private static String checkMarket(int turn)
	{
		advice = null;countOn=null;ban=null;
		countOn = "Markets you should consider, ";
		ban = "Markets you should avoid, ";
		for(int i =0;i<=9;i++)
		{
			if(VariationProbability.getMarketTrend(i)>3)
			{
				
				countOn = " "+countOn +", "+ MarketIndexNameSet(i);
			}else{
				ban = " "+ ban + ", " + MarketIndexNameSet(i); 
			}
		}
		return advice = countOn +" "+ban;
	}
	//################################	CHECK THIS FOR Market Advice 	##########################
	private static String MarketStat(int turn)
	{
		countOn=null;
		advice = null;
		int bal =0;
		countOn = "Markets you should consider, ";
		avoid = "Markets you should avoid, ";
		for(int i=0;i<16;i++)
		{
			bal = VariationProbability.getMarketTrend(turn+1)-VariationProbability.getMarketTrend(turn);
			if(bal >0)
			{
				countOn = " "+countOn +", "+ MarketIndexNameSet(i);
			}else if(bal <0)
			{
				avoid = " "+ avoid + ", " + MarketIndexNameSet(i); 
			}
		}
		return advice = countOn + " "+ avoid;
	}
	//############################################################################################
	//	 	Sector Market Indexing for Analyst to which market should invest which to avoid
	//############################################################################################
	public static String MarketIndexNameSet(int mIndex)
	{
		if(mIndex == 0)
		{
			return "HSBC ";
		}
		else if(mIndex == 1)
		{
			return "LOLC";
		}
		else if(mIndex == 2)
		{
			return "Sampath";
		}
		else if(mIndex == 3)
		{
			return "NSB";
		}else if(mIndex == 4)
		{
			return "Belltaco";
		}else if(mIndex == 5)
		{
			return "McLovin";
		}else if(mIndex == 6)
		{
			return "Kingburger";
		}else if(mIndex == 7)
		{
			return "Hutpizza";
		}else if(mIndex == 8)
		{
			return "98y";
		}else if(mIndex == 9)
		{
			return "Megahard";
		}else if(mIndex == 10)
		{
			return "Labcakes";
		}else if(mIndex == 11)
		{
			return "Kellsjohn";
		}else if(mIndex == 12)
		{
			return "Sony";
		}else if(mIndex == 13)
		{
			return "Vonole";
		}else if(mIndex == 14)
		{
			return "Gateway";
		}else if(mIndex == 15)
		{
			return "HP";
		}
		return "";
	}
}