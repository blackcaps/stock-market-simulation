package com.blackcaps.market_algo;

import org.springframework.web.bind.annotation.RestController;

import com.blackcaps.stock.service.TimerService;


@RestController
public class StockRepository {
	
	static private int counter = 0;
	
	static void marketupdate(int turn)
	{	
		// deleted the old way used list to access all of the different sectors and used a market model to get
		String[] Finance = new String[]{"hsbc","lolc","sampath","nsb"};					
		String[] Consumer = new String[]{"belltaco","mclovin","kingburger","hutpizza"};
		String[] Technology = new String[]{"98y","megahard","labcakes","kellsjohn"};
		String[] Manufacture = new String[]{"sony","vonole","gateway","hp"}; 

		turn = TimerService.getCurrentTurn();
		if(counter == 0)
		{
			VariationProbability.setAll();
			counter=counter+1;
		}
		//#############################################################################################
		//			Remove System.out and access the desired element
		//#############################################################################################
		for(int i =0;i<Finance.length;i++)
		{
			System.out.println(Finance[i]+" "+VariationProbability.getPrices(turn, i));		
		}
		for(int i =4;i<(12-Consumer.length);i++)
		{
			System.out.println(Consumer[i-4]+" "+VariationProbability.getPrices(turn, i));
		}
		for(int i =8;i<(16-Technology.length);i++)
		{
			System.out.println(Technology[i-8]+" "+VariationProbability.getPrices(turn, i));
		}
		for(int i =12;i<(20-Manufacture.length);i++)
		{
			System.out.println(Manufacture[i-12]+" "+VariationProbability.getPrices(turn, i));
		}
	}
				      
}
	
