/**
 * 
 */
package com.blackcaps.market_algo;
import java.util.Random;



/**
 * @author Sadeepa
 *
 */
public final class VariationProbability {
	
	private static int nvoes=0,nxtval=0;
	private static Random trendcal = new Random();
	public static double[][] prices = new double[10][16];
	static double pertot =0;
	//############################################################################################
	//							all trends in array index = turn
	//############################################################################################
	
	private static int[] market_Trend = new int[10];							
	private static int[][] sector_trend = new int[10][4];
	private static int[][] random_Trend = new int[10][16];
			
	//############################################################################################
	//										All Trend Getters
	//############################################################################################
	public static void getMarketTrend() {
		System.out.println("Market Precomputed Trend Values");
		for(int l=0;l<=9;l++)
		{
			System.out.print(market_Trend[l]+" , ");
		}
		System.out.println();
	}
	public static int getMarketTrend(int turn) {
		return market_Trend[turn];
	}
	//GET all the sectors trends or for each turn or for each turn with desired sector
	public static void getSector_T()
	{	System.out.println("Sector Precomputed Trend Values");
		for(int i =0;i<10;i++)
		{
			for(int j =0;j<4;j++)
			{
				System.out.print("Sector Trend "+sector_trend[i][j]+" , "+i+" ");
			}
			System.out.println();
		}
	}
	public static void getSector_T(int turn)
	{
		System.out.println(sector_trend[turn][0]);
		System.out.println(sector_trend[turn][1]);
		System.out.println(sector_trend[turn][2]);
		System.out.println(sector_trend[turn][3]);
	}
	public static int getSector_T(int turn,int j)
	{
		return sector_trend[turn][j];
	}
	//GET Market trends below for each turn or each turn with the desired market
	public static int getRandom_T(int turn,int j)
	{
		return random_Trend[turn][j];
	}
	public static int getRandom_T(int turn)
	{
		for(int o = 0;o<15;o++)
		{
			System.out.println(random_Trend[turn][o]);
		}
		return 0;
	}
	public static void getRandom_T()
	{
		System.out.println("Random Precomputed Trend Values");
		for(int i =0;i<16;i++)
		{
			for(int j =0;j<10;j++)
			{
				System.out.print("Random Trend "+random_Trend[j][i]+" , "+j+" ");
			}
			System.out.println();
		}
	}
	//##########################################################################################
	//above getTrend method have to delete after testing
	//##########################################################################################
		
	//*******************************************************************************************
	
	//					IMPORTANT!!!! neverOverEstimate() method highly used for trend setters
	
	//*******************************************************************************************
	private static int neverOverEstimate(int max,int min,int trendindex,int difference,int currentValue)	
	//never over or underestimate market difference
	{
		nvoes = (int)trendcal.nextInt(100);
		nxtval = currentValue;
		if(trendindex == 0)
		{
			if(nxtval < max && nvoes<25)			//.25 probability of market decrease
			{
				return nxtval=nxtval+difference; 
			}
			else if(nxtval >= min && nvoes<50)		//.25 probability of market increase
			{
				return nxtval=nxtval+difference;
			}else if(nvoes<100)
			{
				return nxtval;						//.5  probability of market not changing
			}					
		}
		else if(trendindex == 1)
		{
			if(nxtval < max && nvoes<25)			//.25 probability of market decrease
			{
				return nxtval=nxtval+difference; 
			}
			else if(nxtval >= min && nvoes<50)		//.25 probability of market increase
			{
				return nxtval=nxtval+difference; 
			}else if(nvoes<100)
			{return nxtval;}					//.5  probability of market not changing
		}
		else if(trendindex == 2)
		{
			if(nxtval < max && nvoes<25)			//.25 probability of market decrease
			{
				return nxtval=nxtval+difference; 
			}
			else if(nxtval >= min && nvoes<50)		//.25 probability of market increase
			{
				return nxtval=nxtval-difference; 
			}else if(nvoes<100)
			{return nxtval;}					//.5  probability of market not changing
		}
		return nxtval;
	}
	//##########################################################################################################
	//Assume 1st 4 are 1st sectors, and 2nd 4 are the second sectors, so on;
	//##########################################################################################################
	//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	//							New Trend Setters unique for each sector,market, :)
	//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	//all the trends in, are generated between -3 to 3 and -2 to 2
	//the pre computed trend then assign to the int array 
	//											THANK YOU!!!
	//##########################################################################################
		
	private static void setmarketTrend()
	{	
		market_Trend[0] = trendcal.nextInt((2 - (-2)) + 1) + (-2);		//random trend set
		for(int w =0;w<9;w++)
		{
			market_Trend[w+1] = neverOverEstimate(2,(-2),0,1,market_Trend[w]);}
	}
	private static void setRandomTrend()
	{
		{for(int i =0;i<16;i++)
		{
			random_Trend[0][i] = trendcal.nextInt((3 - (-3)) + 1) + (-3);}
		}
		for(int o =1;o<10;o++)
		{
			for(int i =0;i<=15;i++)
			{
				random_Trend[o][i] = neverOverEstimate(3,(-3),0,1,random_Trend[o-1][i]);}
		}
	}
	private static void setSector_Trend()
	{
		for(int i =0;i<4;i++)
		{
			sector_trend[0][i] = trendcal.nextInt((3 - (-3)) + 1) + (-3);}
		for(int o =0;o<4;o++)
		{
			for(int i =1;i<10;i++)
			{
				sector_trend[i][o] = neverOverEstimate(3,(-3),0,1,sector_trend[i-1][o]);}
		}
	}

	

	//################################### Remove the Calprices.java file all the operations of it and much more are implemented here 
	//simplified many random trends are now effecting the prices as well as events
	//also all the events are probabilities assign to it 
	//###############################################################################################################################
	private static void SetPriceCal()
	{
		pertot = 0;
		for(int i = 0;i<10;i++)
			{
				for(int j = 0;j<16;j++)
				{
					if(i ==0)
					{
						prices[i][j] = trendcal.nextInt((50 - 40) + 1) + 40;				
						//#############################################################################################################
						//Assume all the stock prices in the stock market starts at a initial random value at turn 1 (between 40 and 50) 
						//#############################################################################################################
								continue;
					}
					if(j<4)
					{	pertot = sector_trend[i][0]+random_Trend[i][j]+market_Trend[i]+EventSet.EventStats(i);
						if(pertot > 0)
						{prices[i][j] = prices[i-1][j]+(100*pertot/100);
						}
						else
						{	prices[i][j] = prices[i-1][j];}
					System.out.println("percentage sum "+pertot);
					System.out.println("Event Stat "+EventSet.EventStats(i));
					}
					else if(j<8)
					{	pertot = sector_trend[i][1]+random_Trend[i][j]+market_Trend[i]+EventSet.EventStats(i);
						if(pertot > 0)
						{prices[i][j] = prices[i-1][j]+(100*pertot/100);
						}
						else
						{	prices[i][j] = prices[i-1][j];}
					}
					else if(j<12)
					{	pertot = sector_trend[i][2]+random_Trend[i][j]+market_Trend[i]+EventSet.EventStats(i);
						if(pertot > 0)
						{prices[i][j] = prices[i-1][j]+(100*pertot/100);
						}
						else
						{	prices[i][j] = prices[i-1][j];}
					}
					else if(j<16)
					{	pertot = sector_trend[i][3]+random_Trend[i][j]+market_Trend[i]+EventSet.EventStats(i);
						if(pertot > 0)
						{prices[i][j] = prices[i-1][j]+(100*pertot/100);
						}
						else
						{	prices[i][j] = prices[i-1][j];}
					}
				}
			}
	}
	//############################################################################################
	// Additional Methods to get each elemental detail to sectors and market
	//############################################################################################
	public static double getPrices(int turn,int index)
	{
		return prices[turn][index];
	}
	public static void showAllprices(int turn)
	{
		for(int j = 0; j<16;j++)
		{System.out.println("From turn "+turn+" Market Prices in order "+j+1+" "+prices[turn][j]);}
	}
	public static void showSectorPrices(int turn,String sector)
	{
		if(sector.equals("Finance"))
		{
			for(int i = 0;i<4;i++)
			{
				System.out.println(sector +" "+prices[turn][i]);
			}
		}
		if(sector.equals("Consumer"))
		{
			for(int i = 4;i<8;i++)
			{
				System.out.println(sector +" "+prices[turn][i]);
			}
		}
		if(sector.equals("Technology"))
		{
			for(int i = 8;i<12;i++)
			{
				System.out.println(sector +" "+prices[turn][i]);
			}
		}
		if(sector.equals("Manufacture"))
		{
			for(int i = 12;i<16;i++)
			{
				System.out.println(sector +" "+prices[turn][i]);
			}
		}
	}
	private static void setTrends()					//limit function exposure 
	{
		setRandomTrend();
		setmarketTrend();
		setSector_Trend();
	}
	public static void setAll()						//only access for trend setter
	{
		setTrends();
		SetPriceCal();
		}
		@SuppressWarnings("unused")		//just for tests
	private static void see1prices()
	{
		for(int i = 0;i<16;i++)
		{
			prices[0][i] = 40;
			System.out.println("Turn 1 price set "+prices[0][i]);
		}
		}
	//#################################### Show prices in a specific turn useful for analyst ###################################
	public static void showMarketPrices(int turn)
	{
		System.out.println("Market Turn =  "+turn);
		for(int i=0;i<16;i++)
		{
			System.out.println("Market Prices =  "+prices[turn][i]);
		}
	}
}	
