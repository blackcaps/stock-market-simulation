package com.blackcaps.stock;

import java.io.IOException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.blackcaps.stock.service.NewTimerService;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

import org.springframework.web.bind.annotation.CookieValue;

@Controller
public class JSPController {
	
	public JSPController() {
		// TODO Auto-generated constructor stub
	}
	
	@GetMapping("/")
	public void getJsp(@CookieValue(value = "randomCook", defaultValue = "0") Long randomCook,
			HttpServletResponse response){
		
//		randomCook += 3;
//	
//		Cookie cookie = new Cookie("randomCook", randomCook.toString());
//		response.addCookie(cookie);
		try {
			response.sendRedirect("/startgame");
		} catch (IOException e) {
			e.printStackTrace();
		}
//		return "index.jsp";
	}
	
	@RequestMapping(value="/startgame")
	public String getHtml(@CookieValue(value="gameid", defaultValue="0") int gameId,
							@CookieValue(value="id", defaultValue="0") int playerId) {
		if(gameId == Integer.parseInt(NewTimerService.GAME_ID) && playerId != 0) {
			return "portfolio.jsp";
		}
		return "welcome_page.jsp";
	}
	
	@GetMapping("/portfolio")
	public String working(@CookieValue(value="gameid") int gameId, HttpServletResponse response) {
		if (NewTimerService.isGameActive() && gameId == Integer.parseInt(NewTimerService.GAME_ID)) {
			return "portfolio.jsp";
		} else {
			try {
				response.sendRedirect("/startgame");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	@GetMapping("/remaining_timer")
	@ResponseBody
	public String getRemainingTime() {
		return Integer.toString(NewTimerService.getRemainingTimeInSeconds());
	}
	
}
