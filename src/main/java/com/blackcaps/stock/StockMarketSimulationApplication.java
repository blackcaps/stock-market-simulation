package com.blackcaps.stock;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JLabel;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import com.blackcaps.algorithm.Variation;
import com.blackcaps.data.Repository;
import com.blackcaps.stock.service.NewTimerService;


@SpringBootApplication
//@ComponentScan({"/","com.blackcaps"})
public class StockMarketSimulationApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = new SpringApplicationBuilder(StockMarketSimulationApplication.class).headless(false).run(args);
		
		EventQueue.invokeLater(() -> {
//			NewTimerService.executeTimer();
//			VariationProbability.setAll();
//			double[][] prices = VariationProbability.getAllPrices();
			
			System.out.println("\n");
			double[][] prices = Variation.initialize();
			for(int i = 0; i < 10; i++) {
				for (int j = 0; j < 16; j++) {
					System.out.print(prices[i][j]);
					System.out.print(" ");
				}
				System.out.println();
			}
			
			Map<Integer, String> map = Repository.getEventStreamNames();
			for (int i = 1; i <= 10; i++) {
				System.out.println("Event for turn " + i + ": " + map.get(i));
			}
			
	        ConfirmationWindow window = context.getBean(ConfirmationWindow.class);
	        
	        JLabel label = new JLabel("<html>Play the game by visiting<br /><b>localhost:9193</b>");
	        label.setFont(new Font("Ariel", Font.PLAIN, 14));
	        window.getContentPane().add(label);
	        
	        window.getContentPane().add(new JLabel(""));
	        window.getContentPane().add(new JLabel("<html>Please close this window when you are<br />finished playing this game.<br />"
					+ "Closing this window will terminate the<br />server and exit the game!"));
	        
	        window.setVisible(true);
	    });
		
		
	}
	
	@SuppressWarnings("serial")
	@SpringBootApplication
	class ConfirmationWindow extends JFrame {
		public ConfirmationWindow() {
			super("Confirmation");
			setSize(300, 200);
			setLayout(new FlowLayout());
			setLocation((int)(Toolkit.getDefaultToolkit().getScreenSize().getWidth()/2)-150, 
					(int)(Toolkit.getDefaultToolkit().getScreenSize().getHeight()/2)-100);
			setDefaultCloseOperation(EXIT_ON_CLOSE);
		}
	}
	
}
