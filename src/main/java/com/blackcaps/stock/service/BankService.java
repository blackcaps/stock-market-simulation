package com.blackcaps.stock.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blackcaps.constant.TransactionType;
import com.blackcaps.data.Repository;
import com.blackcaps.model.Player;
import com.blackcaps.model.Transaction;
import com.blackcaps.model.TransactionAssistant;

@Service
public class BankService {
	
	@Autowired
	private PlayerService playerService;
	
	@Autowired
	private BrokerService brokerService;
	
	private Map<Integer, Transaction> transactions = Repository.getBankTransactions();
	private Map<Integer, Double> bankBalance = Repository.getBank();
	private Map<Integer, Player> players = Repository.getPlayers();
	private Map<Integer, Map<Integer, Integer>> stockQuantityForPlayers = Repository.getStockQuantityForPlayers();
	
	public List<Transaction> getAllTransactions(){
		return new ArrayList<>(transactions.values());
	}
	
	public Transaction getTransactionById(int id) {
		return transactions.get(id);
	}
	
	public void addBankAccount(int playerId, double amount) {
		int player = players.get(playerId).getId();
		bankBalance.put(player, amount);
	}
	
	public void addBankAccout(int playerId) {
		addBankAccount(playerId, 1000.00);
	}
	
	public double getBankBalance(int playerId) {
		return bankBalance.get(playerId);
	}
	
	public List<Transaction> getTransactionForPlayer(int playerId) {
		List<Transaction> transactionsForPlayer = new ArrayList<>();
		
		for(Transaction transaction : transactions.values()) {
			if(transaction.getPlayer().getId() == playerId) {
				transactionsForPlayer.add(transaction);
			}
		}
		
		return transactionsForPlayer;
	}
	
	public int addTransaction(TransactionAssistant transactionAssistant) {
		Transaction transaction = new Transaction(transactions.size() + 1, 
												playerService.getPlayerById(transactionAssistant.getPlayerId()), 
												transactionAssistant.getTransactionType(), 
												transactionAssistant.getAmount(), 
												transactionAssistant.getComment(), 
												TimerService.getCurrentTurn());
		
		transactions.put(transaction.getTransactionId(), transaction);
		
		if(transactionAssistant.getTransactionType() == TransactionType.CREDIT) {
			bankBalance.put(transactionAssistant.getPlayerId(), 
					bankBalance.get(transactionAssistant.getPlayerId()) + transactionAssistant.getAmount());
		} else {
			bankBalance.put(transactionAssistant.getPlayerId(), 
					bankBalance.get(transactionAssistant.getPlayerId()) - transactionAssistant.getAmount());
		}
		
		brokerService.addBrokerTransaction(transaction, transactionAssistant.getSectorId(), 
															transactionAssistant.getStockId(),
															transactionAssistant.getQuantity());
		
		Map<Integer, Integer> playerStockMap = stockQuantityForPlayers.get(transactionAssistant.getPlayerId());
		int stockQuantity = playerStockMap.get(transactionAssistant.getStockId());
		
		playerStockMap.put(transactionAssistant.getStockId(), stockQuantity + transactionAssistant.getQuantity());
		
		stockQuantityForPlayers.put(transactionAssistant.getPlayerId(), playerStockMap);
		
		return transaction.getTransactionId();
	}
}
