package com.blackcaps.stock.service;

import java.util.Timer;
import java.util.TimerTask;

import com.blackcaps.constant.Clock;

public class TimerService {
	private static int currentTurn = 0;
	private static Timer timer = new Timer();

	//starts the clock
	public static void executeTimer() { 
			TimerTask task = new TimerTask() {
			@Override
			public void run() {
				if (currentTurn < Clock.NO_OF_TURNS_DEFAULT) {
					currentTurn++;
					System.out.println("Current turn is: " + currentTurn);
				} else {
					System.out.println("10 turns are over.. The game is exiting..!");
					stopTimer();
				}
			}
		};
		
		timer.schedule(task, 0, Clock.DEFAULT_TURN_SECONDS);
	}
	
	//terminates the timer
	private static void stopTimer() {
		timer.cancel();
	}

	//returns the current turn
	public static int getCurrentTurn() {
		return currentTurn;
	}
	
}

