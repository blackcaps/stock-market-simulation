package com.blackcaps.stock.service;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blackcaps.constant.Clock;

public class NewTimerService {
	//set the amount of time for a single turn here
	private static int selectedTurnTime = Clock.QUICK_TURN_SECONDS;
	//set the number of turns for the game here
	private static int numberOfTurns = Clock.NO_OF_TURNS_DEFAULT;
	
	private static boolean gameIsActive = false;
	
	private static int turnSeconds = selectedTurnTime / 1000;
	private static int currentTurn = 1;
	private static int currentSecondsInTurn = 0;
	
	private static Timer timer = new Timer();
	
	public static String GAME_ID = "0";
	
	static {
		GAME_ID = Integer.toString(new Random().nextInt(1000000) + 1);
	}

	//starts the clock
	public static void executeTimer() { 
			TimerTask task = new TimerTask() {
					
			@Override
			public void run() {
				if (currentTurn <= numberOfTurns) {
					if (currentSecondsInTurn < turnSeconds) {
						currentSecondsInTurn++;
					} else {
						currentTurn++;
						currentSecondsInTurn = 0;
						
						if (currentTurn <= numberOfTurns)
							PriceUpdateService.updatePrices();
					}
				} else {
					stopTimer();
				}
			}
		};
		
		timer.schedule(task, 0, 1000);
		gameIsActive = true;
	}
	
	//terminates the timer
	private static void stopTimer() {
		gameIsActive = false;
		timer.cancel();		
	}

	//returns the current turn
	public static int getCurrentTurn() {
		if (gameIsActive)
			return currentTurn;
		else
			return currentTurn - 1;
	}
	
	public static int getCurrentTimeInSeconds() {
		return turnSeconds * (currentTurn - 1) + currentSecondsInTurn;
	}
	
	public static int getRemainingTimeInSeconds() {
		return numberOfTurns * turnSeconds - getCurrentTimeInSeconds();
	}

	public static boolean isGameActive() {
		return gameIsActive;
	}

	public static void setGameActive(boolean gameIsActive) {
		NewTimerService.gameIsActive = gameIsActive;
	}
	
	
}
