package com.blackcaps.stock.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.blackcaps.data.Repository;
import com.blackcaps.model.Sector;
import com.blackcaps.model.Stock;

@Service
public class SectorService {
	
	private Map<Integer, Sector> sectors = Repository.getSectors();
	
	public List<Sector> getAllSectors() {
		return new ArrayList<>(sectors.values());
	}
	
	public Sector getSectorById(int id) {
		return sectors.get(id);
	}
	
	public Sector getSectorByName(String sectorName) {
		return sectors.entrySet().stream()
				.filter(s -> s.getValue().getName().equals(sectorName))
				.findFirst().get().getValue();
	}
	
	public List<Stock> getStocksForSector(int id) {
		return sectors.get(id).getStocks();
	}
	
	public List<Stock> getStocksForSector(String sectorName) {
		Sector sector = sectors.entrySet().stream()
				.filter(s -> s.getValue().getName().equals(sectorName))
				.findFirst().get().getValue();
		
		return sector.getStocks();
	}
}
