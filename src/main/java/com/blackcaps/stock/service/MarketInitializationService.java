package com.blackcaps.stock.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.blackcaps.data.Repository;
import com.blackcaps.model.Sector;
import com.blackcaps.model.Stock;

@Service
public class MarketInitializationService {
	//Repository or MarketRepository?
	private Map<Integer, Sector> sectors = Repository.getSectors();
	private Map<Integer, Stock> stocks = Repository.getStocks();
	private Map<Integer, List<Double>> stockPriceHistory = Repository.getStockPriceHistory();
//	private double[][] marketPrices = Repository.getMarketPrices();
	
	public int addSector(String sectorName) {
		Sector sector = new Sector(sectors.size() + 1, sectorName, null);
		sectors.put(sector.getId(), sector);
		return sector.getId();
	}
	
	public Sector getSectorById(int id) {
		for (Sector sector : sectors.values()) {
			if (sector.getId() == id) {
				return sector;
			}
		}
		return null;
	}
	
	public Sector getSectorByName(String sectorName) {
		return sectors.entrySet().stream()
				.filter(s -> s.getValue().getName().equals(sectorName))
				.findFirst().get().getValue();
	}
	
	public void addStockToSector(String sectorName, String[] stockNames) {
		Sector sector = sectors.entrySet().stream()
				.filter(s -> s.getValue().getName().equals(sectorName))
				.findFirst().get().getValue();
		
		List<Stock> stocksForSector = new ArrayList<>();
		
		for(String stockName : stockNames) {
			Stock stock = new Stock(stocks.size() + 1, stockName, 0);
			stocks.put(stock.getId(), stock);
			stocksForSector.add(stock);
			
			stockPriceHistory.put(stock.getId(), new ArrayList<Double>());
			double[][] marketPrices = Repository.getMarketPrices();
			List<Double> priceList = stockPriceHistory.get(stock.getId());
			priceList.add(marketPrices[0][stock.getId() - 1]);
		}
		
		sector.setStocks(stocksForSector);
	}
	
	public void addStockToSector(int sectorId, String[] stockNames) {
		Sector sector = getSectorById(sectorId);
		
		List<Stock> stocksForSector = new ArrayList<>();

		for(String stockName : stockNames) {
			Stock stock = new Stock(stocks.size() + 1, stockName, 0);
			stocks.put(stock.getId(), stock);
			stocksForSector.add(stock);
			
			stockPriceHistory.put(stock.getId(), new ArrayList<Double>());
			double[][] marketPrices = Repository.getMarketPrices();
			List<Double> priceList = stockPriceHistory.get(stock.getId());
			priceList.add(marketPrices[0][stock.getId() - 1]);
		}

		sector.setStocks(stocksForSector);
	}
	
}
