package com.blackcaps.stock.service;

import java.util.List;
import java.util.Map;

import com.blackcaps.constant.Stocks;
import com.blackcaps.data.Repository;

public class PriceUpdateService {
	
	private static Map<Integer, List<Double>> stockPriceHistory = Repository.getStockPriceHistory();
//	private static double[][] marketPrices = Repository.getMarketPrices();
	
	public static void updatePrices() {
		double[][] marketPrices = Repository.getMarketPrices();
		int currentTurn = NewTimerService.getCurrentTurn();
		
		for (int i = 1; i <= Stocks.NO_OF_STOCKS_DEFAULT; i++) {
			List<Double> priceListForStock = stockPriceHistory.get(i);
			priceListForStock.add(marketPrices[currentTurn - 1][i - 1]);
		}
	}
}
