package com.blackcaps.stock.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.blackcaps.data.Repository;
import com.blackcaps.model.Sector;
import com.blackcaps.model.Stock;

@Service
public class StockService {
	
	private Map<Integer, Stock> stocks = Repository.getStocks();
	private Map<Integer, Sector> sectors = Repository.getSectors();
	
	public List<Stock> getAllStocks() {
		return new ArrayList<>(stocks.values());
	}
	
	public List<Stock> getStocksForSector(int sectorId) {		
		Sector sector = sectors.get(sectorId);
		return sector.getStocks();
	}
	
	public List<Stock> getStocksForSector(String sectorName) {
		Sector sector = sectors.entrySet().stream()
				.filter(s -> s.getValue().getName().equals(sectorName))
				.findFirst().get().getValue();
		
		return sector.getStocks();
	}
	
	public Stock getStockById(int stockId) {
		return stocks.get(stockId);
	}
	
	public Stock getStockByName(String stockName) {
		return stocks.entrySet().stream()
				.filter(s -> s.getValue().getName().equals(stockName))
				.findFirst().get().getValue();
	}
	
	public String[] getAllStockNames() {
		String[] stockList = new String[stocks.size()];
		
		for (int i = 0; i < stocks.size(); i++) {
			stockList[i] = stocks.get(i + 1).getName();
		}
		
		return stockList;
	}
	
	public String[] getStockNamesForSector(int sectorId) {
		List<String> stocksNamesForSector = new ArrayList<>();		
		List<Stock> stockList = getStocksForSector(sectorId);
		
		for(Stock stock : stockList) {
			stocksNamesForSector.add(stock.getName());
		}
		
		String[] stockNames = new String[stocksNamesForSector.size()];
		return stocksNamesForSector.toArray(stockNames);
	}
	
	public String[] getStockNamesForSector(String sectorName) {
		List<String> stocksNamesForSector = new ArrayList<>();
		List<Stock> stockList = getStocksForSector(sectorName);
		
		for(Stock stock : stockList) {
			stocksNamesForSector.add(stock.getName());
		}

		String[] stockNames = new String[stocksNamesForSector.size()];
		return stocksNamesForSector.toArray(stockNames);
	}
}
