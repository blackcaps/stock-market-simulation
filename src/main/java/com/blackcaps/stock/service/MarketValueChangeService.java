package com.blackcaps.stock.service;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.blackcaps.data.Repository;
import com.blackcaps.model.Sector;
import com.blackcaps.model.Stock;

@Service
public class MarketValueChangeService {
	//Repository or MarketRepository
	private Map<Integer, Sector> sectors = Repository.getSectors();
	private Map<Integer, Stock> stocks = Repository.getStocks();
	
	public void changeStockPrice(String stockName, double rate) {
		Stock stock = stocks.entrySet().stream()
				.filter(s -> s.getValue().getName().equals(stockName))
				.findFirst().get().getValue();
		
		stock.setPrice(stock.getPrice() + rate);
	}
	
	public void changeStockPrice(int stockId, double rate) {
		stocks.get(stockId).setPrice(stocks.get(stockId).getPrice() + rate);
	}
	
	public void setStockPrice(int stockId, double price) {
		stocks.get(stockId).setPrice(price);
	}
	
	public void changeSectorPrice(String sectorName, double rate) {
		Sector sector = sectors.entrySet().stream()
				.filter(s -> s.getValue().getName().equals(sectorName))
				.findFirst().get().getValue();
		
		for (Stock stock : sector.getStocks()) {
			stock.setPrice(stock.getPrice() + rate);
		}
	}
	
	public void changeSectorPrice(int sectorId, double rate) {
		for (Stock stock : sectors.get(sectorId).getStocks()) {
			stock.setPrice(stock.getPrice() + rate);
		}
	}
	
	public void changeMarketPrice(double rate) {
		for (Stock stock : stocks.values()) {
			stock.setPrice(stock.getPrice() + rate);
		}
	}
	
}
