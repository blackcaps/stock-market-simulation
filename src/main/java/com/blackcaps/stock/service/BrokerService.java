package com.blackcaps.stock.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blackcaps.data.Repository;
import com.blackcaps.model.Broker;
import com.blackcaps.model.Transaction;

@Service
public class BrokerService {
	
	@Autowired
	private SectorService sectorService;
	
	@Autowired
	private StockService stockService;

	private Map<Integer, Broker> broker = Repository.getBroker();
	
	public List<Broker> getBrokerTransactionsForPlayer(int playerId) {
		List<Broker> brokerTransactionsForPlayer = new ArrayList<>();
		
		for (Broker currentBroker : broker.values()) {
			if(currentBroker.getPlayer().getId() == playerId) {
				brokerTransactionsForPlayer.add(currentBroker);
			}
		}
		
		return brokerTransactionsForPlayer;
	}
	
	public void addBrokerTransaction(Transaction transaction, int sectorId, int stockId, int quantity) {
		Broker curentBroker = new Broker(broker.size() + 1, 
									transaction.getPlayer(), 
									transaction, 
									sectorService.getSectorById(sectorId), 
									stockService.getStockById(stockId), 
									transaction.getTransactionType(), 
									stockService.getStockById(stockId).getPrice(),
									quantity);
		
		broker.put(curentBroker.getId(), curentBroker);
	}
}
