package com.blackcaps.stock.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.blackcaps.constant.Stocks;
import com.blackcaps.data.Repository;
import com.blackcaps.model.Player;

@Service
public class PlayerService {
	
	private Map<Integer, Player> players = Repository.getPlayers();
	private Map<Integer, Map<Integer, Integer>> stockQuantityForPlayers = Repository.getStockQuantityForPlayers();
	
	public List<Player> getAllPlayers() {
		return new ArrayList<Player>(players.values());
	}
	
	public Player getPlayerById(int id) {
		return players.get(id);
	}
	
	public Player getPlayerByName(String name) {
		return players.entrySet().stream().filter(s -> s.getValue().getName().equals(name)).findFirst().get().getValue();
	}
	
	public void addPlayer(Player player) {
		player.setId(players.size() + 1);
		players.put(player.getId(), player);
		
		Map<Integer, Integer> quantityMap = new HashMap<>();
		//stock id starts from 0
		for (int i = 0; i < Stocks.NO_OF_STOCKS_DEFAULT; i++) {
			quantityMap.put(i, 0);
		}
		stockQuantityForPlayers.put(player.getId(), quantityMap);
	}
	
	public int addPlayer(String playerName) {
		Player player = new Player(players.size() + 1, playerName);
		players.put(player.getId(), player);
		
		Map<Integer, Integer> quantityMap = new HashMap<>();
		//stock id starts from 0
		for (int i = 0; i < Stocks.NO_OF_STOCKS_DEFAULT; i++) {
			quantityMap.put(i + 1, 0);
		}
		stockQuantityForPlayers.put(player.getId(), quantityMap);
		
		return player.getId();
	}
}
