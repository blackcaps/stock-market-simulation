package com.blackcaps.stock.resource;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.blackcaps.model.BankAssistant;
import com.blackcaps.model.Player;
import com.blackcaps.stock.service.BankService;
import com.blackcaps.stock.service.NewTimerService;
import com.blackcaps.stock.service.PlayerService;

@Controller
public class PlayerResource {
	
	@Autowired
	PlayerService playerService;
	
	@Autowired
	BankService bankService;
	
	@RequestMapping(method=RequestMethod.GET, value="/createplayer/{playerName}")
	@ResponseBody
	public void create(@PathVariable String playerName, HttpServletResponse response) {
		
		int playerId = playerService.addPlayer(playerName);
		
		bankService.addBankAccout(playerId);
		
		Cookie playerCookie = new Cookie("player", playerName);
		Cookie idCookie = new Cookie("id", String.valueOf(playerId));
		Cookie gameCookie = new Cookie("gameid", NewTimerService.GAME_ID);
		playerCookie.setPath("/");
		idCookie.setPath("/");
		gameCookie.setPath("/");
		playerCookie.setMaxAge(NewTimerService.getRemainingTimeInSeconds() + 60 * 3);
		idCookie.setMaxAge(NewTimerService.getRemainingTimeInSeconds() + 60 * 3);
		gameCookie.setMaxAge(NewTimerService.getRemainingTimeInSeconds() + 60 * 3);
				
		response.addCookie(playerCookie);
		response.addCookie(idCookie);
		response.addCookie(gameCookie);
		
		try {
			response.sendRedirect("/portfolio");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
//		return "created";
	}
	
	@RequestMapping("/playerdetail/{playerId}")
	@ResponseBody
	public BankAssistant getPlayerBankDetails(@PathVariable int playerId, HttpServletResponse response) {
		Player player = playerService.getPlayerById(playerId);
		double amount = bankService.getBankBalance(playerId);
		
		return new BankAssistant(player.getName(), amount);
	}
	
		
	@RequestMapping("/getplayer/{id}")
	@ResponseBody
	public Player getPlayer(@PathVariable int id) {
		return playerService.getPlayerById(id);
	}
	
	@RequestMapping("/getplayers")
	@ResponseBody
	public List<Player> getAllPlayers() {
		return playerService.getAllPlayers();
	}
	
	@RequestMapping("/cookie")
	@ResponseBody
	public String getCookie(@CookieValue(value = "player") String playerCookie,
							@CookieValue(value = "gameid") int gameId) {
		String string = "player cookie : " + playerCookie + " <br />game id cookie : " + gameId;
		System.out.println(string);
		return string;
	}
	
	//for redirecting
//	 @RequestMapping("/foo")
//	  void handleFoo(HttpServletResponse response) throws IOException {
//	    response.sendRedirect("some-url");
//	  }
}
