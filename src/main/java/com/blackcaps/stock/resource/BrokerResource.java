package com.blackcaps.stock.resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BrokerResource {
	
	@RequestMapping("/broker")
	public String getBrokerPage() {
		return "broker.jsp";
	}
}
