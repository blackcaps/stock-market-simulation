package com.blackcaps.stock.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import com.blackcaps.stock.service.BankService;

@Controller
public class BankResource {
	
	@Autowired
	BankService bankService;
	
	@GetMapping("/balance/{playerId}")
	@ResponseBody
	public String getBankBalance(@PathVariable int playerId) {
		return Double.toString(bankService.getBankBalance(playerId));
	}
}
