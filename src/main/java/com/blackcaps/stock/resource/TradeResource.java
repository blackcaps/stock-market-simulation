package com.blackcaps.stock.resource;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.blackcaps.constant.Stocks;
import com.blackcaps.constant.TransactionType;
import com.blackcaps.data.Repository;
import com.blackcaps.model.Bank;
import com.blackcaps.model.BuySellAssistant;
import com.blackcaps.model.Player;
import com.blackcaps.model.Sample;
import com.blackcaps.model.Sector;
import com.blackcaps.model.Stock;
import com.blackcaps.model.Transaction;
import com.blackcaps.model.TransactionAssistant;
import com.blackcaps.stock.service.BankService;
import com.blackcaps.stock.service.NewTimerService;
import com.blackcaps.stock.service.PlayerService;
import com.blackcaps.stock.service.SectorService;
import com.blackcaps.stock.service.StockService;

@Controller
public class TradeResource {
	
	@Autowired
	PlayerService playerService;
	
	@Autowired
	SectorService sectorService;
	
	@Autowired
	StockService stockService;
	
	@Autowired
	BankService bankService;
	
	DecimalFormat decimalFormat = new DecimalFormat("0.00");
	
	private Map<Integer, Player> players = Repository.getPlayers();
	private Map<Integer, Double> bank = Repository.getBank();
	private Map<Integer, Stock> stocks = Repository.getStocks();
	private Map<Integer, Transaction> transactions = Repository.getBankTransactions();
	private Map<Integer, Map<Integer, Integer>> stockQuantityForPlayers = Repository.getStockQuantityForPlayers();
	
	@GetMapping("/trade")
	public String getTradePage() {
		return "trade.jsp";
	}
	
	@GetMapping("tjson")
	@ResponseBody
	public List<Bank> getBankDetails() {
		Player player = new Player(101, "arsha");
		Player player2 = new Player(102, "prof");
		Player player3 = new Player(103, "sartar");
		Player player4 = new Player(104, "arathon");
		
		Bank bank = new Bank(player, 1002.45);
		Bank bank2 = new Bank(player3, 973.0);
		
		List<Bank> list = new ArrayList<>();
		list.add(bank);
		list.add(bank2);
		
		return list;
	}
	
	@RequestMapping(value="/addtransaction", method=RequestMethod.POST)
	@ResponseBody
	public String addTransaction(@RequestBody BuySellAssistant transaction,
								@CookieValue(value="player", defaultValue="0") String playerCookie,
								@CookieValue(value="id", defaultValue="0") int playerId,
								@CookieValue(value="gameid") int gameId) {
		BuySellAssistant buySell = transaction;
		
		if (gameId != Integer.parseInt(NewTimerService.GAME_ID) || playerId == 0) {
			return "failed";
		}
		
		Player player = players.get(playerId);
		Sector sector = sectorService.getSectorByName(buySell.getSector());
		Stock stock = stockService.getStockByName(buySell.getStock());
		
		int transactionType = buySell.getTransaction().equalsIgnoreCase("buy") ? TransactionType.BUY : TransactionType.SELL;
		double total = buySell.getQuantity() * stock.getPrice();
		
		if (transactionType == TransactionType.BUY) {
			
			if (bank.get(player.getId()) < total) {
				return "Insufficient balance in your account!";
			}
			
			TransactionAssistant ta = new TransactionAssistant(playerId, transactionType, total,
							"Player " + player.getName() + " bought " + buySell.getQuantity() 
							+ " quantities of " + buySell.getStock() + " for " + " Rs. " 
							+ decimalFormat.format(total), sector.getId(), stock.getId(), buySell.getQuantity());
			
			bankService.addTransaction(ta);
			
			return "Transaction successful!";
		} else {
			TransactionAssistant ta = new TransactionAssistant(playerId, transactionType, total,
					"Player " + player.getName() + " bought " + buySell.getQuantity() 
					+ " quantities of " + buySell.getStock() + " for " + " Rs. " 
					+ decimalFormat.format(total), sector.getId(), stock.getId(), buySell.getQuantity());
	
			bankService.addTransaction(ta);
		}		
		
//		System.out.println("Sector : " + buySell.getSector());
//		System.out.println("Stock : " + buySell.getStock());
//		System.out.println("Transaction : " + buySell.getTransaction());
//		System.out.println("Quantity : " + buySell.getQuantity());
//		
//		System.out.println();
//		System.out.println(Repository.getPlayers().get(playerId).getName());
//		System.out.println("Player ID : " + playerCookie);
//		System.out.println("Game ID : " + gameId);
		
		return "success";
	}
	
	@RequestMapping("/sampletest")
	@ResponseBody
	public List<Sample> getBanks() {
//		Bank bank = new Bank(new Player(101, "ars"), 1002.0);
//		Bank bank2 = new Bank(new Player(102, "brs"), 985.0);
		
		Sample sample = new Sample("arsh", 75.0, 95.0);
		Sample sample2 = new Sample("brs", 34.0, 25);
		Sample sample3 = new Sample("dfs", 71.0, 45.0);
		
		List<Sample> list = new ArrayList<>();
		list.add(sample);
		list.add(sample2);
		list.add(sample3);
		
		return list;
	}
	
	
	
}
