package com.blackcaps.stock.resource;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.blackcaps.algorithm.Variation;
import com.blackcaps.constant.Sectors;
import com.blackcaps.constant.Stocks;
import com.blackcaps.data.Repository;
import com.blackcaps.model.Stock;
import com.blackcaps.stock.service.MarketInitializationService;
import com.blackcaps.stock.service.NewTimerService;
import com.blackcaps.stock.service.StockService;

@Controller
public class GameResource {
	
	@Autowired
	private StockService stockService;
	
	@Autowired
	private MarketInitializationService marketInitializationService;
	
	@RequestMapping("/creategame")
	@ResponseBody
	public void createGame(HttpServletResponse response) {
		NewTimerService.executeTimer();
		Repository.setMarketPrices(Variation.initialize());
		
		int sector1 = marketInitializationService.addSector(Sectors.FINANCE);
		int sector2 = marketInitializationService.addSector(Sectors.CONSUMER);
		int sector3 = marketInitializationService.addSector(Sectors.TECHNOLOGY);
		int sector4 = marketInitializationService.addSector(Sectors.MANUFACTURING);
		
		marketInitializationService.addStockToSector(sector1, new String[] {Stocks.HSBC, Stocks.LOLC, Stocks.SAMPATH, Stocks.NSB});
		marketInitializationService.addStockToSector(sector2, new String[] {Stocks.BELLTACO, Stocks.MCLOVIN, Stocks.KINGBURGER, Stocks.HUTPIZZA});
		marketInitializationService.addStockToSector(sector3, new String[] {Stocks.Y98, Stocks.MEGAHARD, Stocks.LABCAKES, Stocks.KELLSJOHN});
		marketInitializationService.addStockToSector(sector4, new String[] {Stocks.SONY, Stocks.VONOLE, Stocks.GATEWAY, Stocks.HP});
		
//		
//		try {
//			response.sendRedirect("/startgame");
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
	}
	
	@RequestMapping("/getstocks")
	@ResponseBody
	public List<Stock> getStocks(){		
		return stockService.getAllStocks();
	}
	
	@RequestMapping("/getstocks/{sectorId}")
	@ResponseBody
	public List<Stock> getStocksForSector(@PathVariable int sectorId) {
		return stockService.getStocksForSector(sectorId);
	}
	
	@RequestMapping("/getstocknames/{sectorId}")
	public List<String> getStockNamesForSector(@PathVariable int sectorId) {
		return Arrays.asList(stockService.getStockNamesForSector(sectorId));
	}
}
