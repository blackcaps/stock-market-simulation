package com.blackcaps.stock.resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.blackcaps.constant.Clock;
import com.blackcaps.stock.service.NewTimerService;

@Controller
public class TimerResource {
	@RequestMapping("/remaining_time")
	@ResponseBody
	public String getRemainingTime() {
		int time = NewTimerService.getRemainingTimeInSeconds();
		if (time > 0)
			return Integer.toString(time);
		return "Game Over!";
	}
	
	@RequestMapping("/gameid")
	@ResponseBody
	public String getGameId() {
		return NewTimerService.GAME_ID;
	}
	
	@RequestMapping("/get_turn")
	@ResponseBody
	public String getTurn() {
		int turn;
		turn = NewTimerService.getCurrentTurn();
		if (turn > Clock.NO_OF_TURNS_DEFAULT)
			turn = Clock.NO_OF_TURNS_DEFAULT;
		
		return Integer.toString(turn);
	}
}
