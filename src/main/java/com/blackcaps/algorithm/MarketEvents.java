package com.blackcaps.algorithm;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import com.blackcaps.data.Repository;

public class MarketEvents {
	
	private static final int STOCK_SIZE = 16;
	private static final int NO_OF_TURNS = 10;

	public static final int BOOM = 0;
	public static final int BUST = 1;
	
	public static final int PROFIT_WARNING = 2;
	public static final int TAKE_OVER = 3;
	public static final int SCANDAL = 4;
	
	public static final int SECTOR_EVENT_DURATION_MIN = 2;
	public static final int SECTOR_EVENT_DURATION_MAX = 5;
	
	public static final int STOCK_EVENT_DURATION_MIN = 1;
	public static final int STOCK_EVENT_DURATION_MAX = 7;
	
	private static int[] sectorStartPositions;
	private static int[] eventDurations = new int[NO_OF_TURNS];
	private static int[] eventProbabilities = new int[] {10, 10, 10, 10, 10, 10};
	
	private static double[][] eventStream = new double[NO_OF_TURNS][STOCK_SIZE];
	private static Map<Integer, String> eventNames = new HashMap<>();
	
	private static Random probability = new Random();
	private static DecimalFormat decimalFormat = new DecimalFormat("0.00");
	
	static double[][] getEventStream(int[] sectors) {
		sectorStartPositions = sectors;		
		getEventType();
		
		for(int i = 0; i < NO_OF_TURNS; i++) {
			for (int j = 0; j < STOCK_SIZE; j++) {
				eventStream[i][j] = Double.parseDouble(decimalFormat.format(eventStream[i][j]));
			}
		}
		Repository.setEventStreamNames(eventNames);
		return eventStream;
	}
	
	private static void getEventType() {
		for (int i = 0; i < NO_OF_TURNS; i++) {
			int random = probability.nextInt(100);
			if (random < 33) {
				random = probability.nextInt(eventProbabilities[0] + eventProbabilities[1]);
				if (random < eventProbabilities[0]) {
					for (int n = 1; n < 6; n++) {
						eventProbabilities[n] = eventProbabilities[n] + 10;
					}
					eventProbabilities[0] = 0;					
					generateEventStream(new int[] {BOOM, i, 1, 5});
					eventNames.put(i + 1, "BOOM");
					
				} else {
					for (int n = 0; n < 6; n++) {
						if (n == 0 || n == 2 || n == 3) {							//comment this line
							eventProbabilities[n] = eventProbabilities[n] + 20; 	//comment this line		
						}															//comment this line
						eventProbabilities[n] = eventProbabilities[n] + 10;
					}
					eventProbabilities[1] = 0;					
					generateEventStream(new int[] {BUST, i, -1, -5});
					eventNames.put(i + 1, "BUST");
					
				}
			} else {
				random = probability.nextInt(eventProbabilities[2] + eventProbabilities[3] + eventProbabilities[4] + eventProbabilities[5]);
				if (random < eventProbabilities[2] + eventProbabilities[3]) {
					for (int n = 0; n < 6; n++) {
						eventProbabilities[n] = eventProbabilities[n] + 10;
					}
					eventProbabilities[2] = 0;
					eventProbabilities[3] = 0;
					generateEventStream(new int[] {PROFIT_WARNING, i, 2, 3});
					eventNames.put(i + 1, "PROFIT_WARNING");
					
				} else if (random < eventProbabilities[2] + eventProbabilities[3] + eventProbabilities[4]) {
					for (int n = 0; n < 6; n++) {
						if (n == 0 || n == 2 || n == 3)	{							//comment this line
							eventProbabilities[n] = eventProbabilities[n] + 15; 	//comment this line	
						}															//comment this line
						eventProbabilities[n] = eventProbabilities[n] + 10;
					}
					eventProbabilities[4] = 0;
					generateEventStream(new int[] {TAKE_OVER, i, -1, -5});
					eventNames.put(i + 1, "TAKE_OVER");
					
				} else {
					for (int n = 0; n < 6; n++) {
						if (n == 0 || n == 2 || n == 3) {							//comment this line
							eventProbabilities[n] = eventProbabilities[n] + 20; 	//comment this line
						}															//comment this line
						eventProbabilities[n] = eventProbabilities[n] + 10;
					}
					eventProbabilities[5] = 0;					
					generateEventStream(new int[] {SCANDAL, i, -3, -6});
					eventNames.put(i + 1, "SCANDAL");
				}
			}
		}
	}
	
	//eventType[0] -> event type, eventType[1] -> turn no, 
	//eventType[2] -> minimum score, eventType[3] -> maximum score
	private static void generateEventStream(int[] eventType) {
		if (eventType[0] == BOOM || eventType[0] == BUST) {
			int duration = probability.nextInt(5 + 1) + 2;
			int turn = eventType[1];
			
			eventDurations[turn] = duration;
			
			//test
//			System.out.println("Turn: " + eventType[1] + "  duration: " + duration);
			
		} else {
			int duration = probability.nextInt(7) + 1;
			int turn = eventType[1];
			
			eventDurations[turn] = duration;
			
			//test
//			System.out.println("Turn: " + eventType[1] + "  duration: " + duration);
		}
		
		
		if (eventType[0] == BOOM || eventType[0] == BUST) {
			int sectorPosition = probability.nextInt(sectorStartPositions.length);

			int sectorStartPosition = sectorStartPositions[sectorPosition];
			int sectorEndPosition;
			if (sectorPosition == sectorStartPositions.length - 1) {
				sectorEndPosition = eventStream[0].length - 1;
			} else {
				sectorEndPosition = sectorStartPositions[sectorPosition + 1] - 1;
			}
			
//			System.out.println("Sector start position: " + sectorStartPosition);
//			System.out.println("Sector end position: " + sectorEndPosition);
			
			//eventType[0] is type of event, eventType[1] is turn number
			if (eventType[0] == BOOM) {
//				int eventValue = probability.nextInt(5) + 1;
				double eventValue = ThreadLocalRandom.current().nextDouble(1.0, 5.0);
//				System.out.println("Event value: " + eventValue);
				for (int k = 0; k < eventDurations[eventType[1]]; k++) {
					for (int i = sectorStartPosition; i < sectorEndPosition + 1; i++) {
						if (eventType[1] + k < NO_OF_TURNS) {
							eventStream[eventType[1] + k][i] = eventStream[eventType[1] + k][i] + eventValue;			
						}
					}
				}
			} else {
//				int eventValue = probability.nextInt(5) - 5;
				double eventValue = ThreadLocalRandom.current().nextDouble(-5.0, -1.0);
//				System.out.println("Event value: " + eventValue);
				for (int k = 0; k < eventDurations[eventType[1]]; k++) {
					for (int i = sectorStartPosition; i < sectorEndPosition + 1; i++) {
						if (eventType[1] + k < NO_OF_TURNS) {
							eventStream[eventType[1] + k][i] = eventStream[eventType[1] + k][i] + eventValue;	
						}
					}
				}
			}			
		} else { //event type = profit warning | take over | scandal
			int randomStock = probability.nextInt(STOCK_SIZE);
//			System.out.println("Stock index is: " + randomStock);
			
			if (eventType[0] == PROFIT_WARNING) {
//				int eventValue = probability.nextInt(2) + 2;
				double eventValue = ThreadLocalRandom.current().nextDouble(2.0, 3.0);
//				System.out.println("Event value: " + eventValue);
				
				for (int k = 0; k < eventDurations[eventType[1]]; k++) {
					if (eventType[1] + k < NO_OF_TURNS) {
						eventStream[eventType[1] + k][randomStock] = eventStream[eventType[1] + k][randomStock] + eventValue;
					}
				}
			} else if (eventType[0] == TAKE_OVER) {
//				int eventValue = probability.nextInt(5) - 5;
				double eventValue = ThreadLocalRandom.current().nextDouble(-5.0, -1.0);
//				System.out.println("Event value: " + eventValue);
				
				for (int k = 0; k < eventDurations[eventType[1]]; k++) {
					if (eventType[1] + k < NO_OF_TURNS) {
						eventStream[eventType[1] + k][randomStock] = eventStream[eventType[1] + k][randomStock] + eventValue;
					}
				}
			} else {
//				int eventValue = probability.nextInt(4) - 6;
				double eventValue = ThreadLocalRandom.current().nextDouble(-6.0, -3.0);
//				System.out.println("Event value: " + eventValue);
				
				for (int k = 0; k < eventDurations[eventType[1]]; k++) {
					if (eventType[1] + k < NO_OF_TURNS) {
						eventStream[eventType[1] + k][randomStock] = eventStream[eventType[1] + k][randomStock] + eventValue;
					}
				}
			}
		}
		
//		for (int i = 0; i < NO_OF_TURNS; i++) {
//			for (int j = 0; j < STOCK_SIZE; j ++) {
//				System.out.print(decimalFormat.format(eventStream[i][j]) + "  ");
//			}
//			System.out.println();
//		}
//		System.out.println();
	}
}
