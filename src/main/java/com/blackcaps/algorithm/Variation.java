package com.blackcaps.algorithm;

import java.text.DecimalFormat;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import com.blackcaps.constant.Clock;

public class Variation {
	private static final double STARTING_PRICE_MIN = 10.0;
	private static final double STARTING_PRICE_MAX = 40.0;
	
	private static final int STOCK_SIZE = 16;
	private static final int NO_OF_TURNS = Clock.NO_OF_TURNS_DEFAULT; //10
	
	private static int[] sectorStartPositions;
	private static double[][] stockPrices = new double[NO_OF_TURNS][STOCK_SIZE];
	private static double[][] randomTrend = new double[NO_OF_TURNS][STOCK_SIZE];
	private static double[][] sectorTrend = new double[NO_OF_TURNS][STOCK_SIZE];
	private static double[][] generalMarketTrend = new double[NO_OF_TURNS][STOCK_SIZE];
	
	private static double[][] eventStream = new double[NO_OF_TURNS][STOCK_SIZE];
	
	private static double[][] finalStockPrices = new double[NO_OF_TURNS][STOCK_SIZE];
	
	
	private static DecimalFormat decimalFormat = new DecimalFormat("0.00");
	
	private static void generateStockPrices() {
		for (int i = 0; i < STOCK_SIZE; i++) {
			stockPrices[0][i] = Double.parseDouble(decimalFormat
							.format(ThreadLocalRandom.current()
							.nextDouble(STARTING_PRICE_MIN, STARTING_PRICE_MAX)));
		}
	}
	
	private static void generateRandomTrend() {
		for (int i = 0; i < NO_OF_TURNS; i++) {
			for (int j = 0; j < STOCK_SIZE; j++) {
				randomTrend[i][j] = Double.parseDouble(decimalFormat
								.format(ThreadLocalRandom.current()
								.nextDouble(-2, 2)));
//				System.out.print(randomTrend[i][j] + "  ");
			}
			
//			Print statements for testing purposes
//			System.out.println();
		}
	}
	
	private static void generateSectorTrend() {
		int pos = 1;
		int sectorNextPosition = sectorStartPositions[pos];
		double tempValue = 0.0;
		Random probability = new Random();
		
		for (int i = 0; i < NO_OF_TURNS; i++) {
			boolean valueGenerated = false;
			for (int j = 0; j < STOCK_SIZE; j++) {
				if (j < sectorNextPosition) {
					if (!valueGenerated) {
						if (i != 0 && (probability.nextInt(100) < 50)) {
							tempValue = sectorTrend[i - 1][j];
						}
						else {
//							tempValue = Double.parseDouble(decimalFormat.format(ThreadLocalRandom.current().nextDouble(-3, 3))); //uncomment this
							if (probability.nextInt(100) < 80)									//comment this
								tempValue = ThreadLocalRandom.current().nextDouble(0.0, 3.0);	//comment this
							else																//comment this
								tempValue = ThreadLocalRandom.current().nextDouble(-3.0, 0.0);	//comment this
							
							if (i != 0) {
								if (tempValue < sectorTrend[i - 1][j] - 1)
									tempValue = sectorTrend[i - 1][j] - 1;
								else if (tempValue > sectorTrend[i - 1][j] + 1)
									tempValue = sectorTrend[i - 1][j] + 1;
							}
						}
						valueGenerated = true;
					}
					sectorTrend[i][j] = Double.parseDouble(decimalFormat.format(tempValue));
				} else {
					if (i != 0 && (probability.nextInt(100) < 50)) {
						tempValue = sectorTrend[i - 1][j];
					}
					else {
//						tempValue = Double.parseDouble(decimalFormat.format(ThreadLocalRandom.current().nextDouble(-3, 3))); //uncomment this
						if (probability.nextInt(100) < 80)									//comment this
							tempValue = ThreadLocalRandom.current().nextDouble(0.0, 3.0);	//comment this
						else																//comment this
							tempValue = ThreadLocalRandom.current().nextDouble(-3.0, 0.0);	//comment this
						
						if (i != 0) {
							if (tempValue < sectorTrend[i - 1][j] - 1)
								tempValue = sectorTrend[i - 1][j] - 1;
							else if (tempValue > sectorTrend[i - 1][j] + 1)
								tempValue = sectorTrend[i - 1][j] + 1;
						}
					}
					
					sectorTrend[i][j] = Double.parseDouble(decimalFormat.format(tempValue));
					
					if (sectorStartPositions.length > pos + 1) {
						sectorNextPosition = sectorStartPositions[++pos];
					} else {
						sectorNextPosition = sectorTrend[0].length;
					}
				}
			}
			pos = 1;
			sectorNextPosition = sectorStartPositions[pos];
		}
		
//		Print statements for testing purposes
//		for (int i = 0; i < NO_OF_TURNS; i++) {
//			for (int j = 0; j < STOCK_SIZE; j++) {
//				System.out.print(decimalFormat.format(sectorTrend[i][j]) + "  ");
//			}
//			System.out.println();
//		}
	}
	
	private static void generateGeneralMarketTrend() {
		Random probability = new Random();
		double[] marketTrend = new double[NO_OF_TURNS];

		for (int i = 0; i < NO_OF_TURNS; i++) {			
			if (probability.nextInt(100) < 50) {
				if (i == 0) {
//					marketTrend[i] = ThreadLocalRandom.current().nextDouble(-3, 3);		//uncomment this
					marketTrend[i] = ThreadLocalRandom.current().nextDouble(1.1, 1.5); 	//comment this
				} else {
					marketTrend[i] = marketTrend[i - 1];
				}
			} else {
				if (i == 0) {															//comment this
					marketTrend[i] = ThreadLocalRandom.current().nextDouble(1.1, 1.5);	//comment this
				} else {																//comment this
					marketTrend[i] = ThreadLocalRandom.current().nextDouble(-3, 3);
				}																		//comment this
				
				if (i != 0) {
					if (marketTrend[i] > marketTrend[i - 1] + 1) {
						marketTrend[i] = marketTrend[i - 1] + 1;
					} else if (marketTrend[i] < marketTrend[i - 1] - 1) {
						marketTrend[i] = marketTrend[i - 1] - 1;
					}
				}
			}
			
			for (int j = 0; j < STOCK_SIZE; j++) {
				generalMarketTrend[i][j] = Double.parseDouble(decimalFormat.format(marketTrend[i]));
			}
		}
		
//		Print statements for testing purposes
//		for (int i = 0; i < NO_OF_TURNS; i++) {
//			for (int j = 0; j < STOCK_SIZE; j++) {
//				System.out.print(Double.parseDouble(decimalFormat.format(generalMarketTrend[i][j])) + "  ");
//			}
//			System.out.println();
//		}
	}
	
	private static void addTrendsAndEvents() {
		for (int i = 0; i < NO_OF_TURNS; i++) {
			for (int j =0; j < STOCK_SIZE; j++) {
				stockPrices[i][j] = stockPrices[0][j];
			}
		}
		
		for (int n = 0; n < STOCK_SIZE; n++) {
			finalStockPrices[0][n] = stockPrices[0][n];
		}
		
		for (int i = 1; i < NO_OF_TURNS; i++) {
			for (int j = 0; j < STOCK_SIZE; j++) {				
				finalStockPrices[i][j] = Double.parseDouble(decimalFormat.format(stockPrices[i][j] 
						+ randomTrend[i][j] + sectorTrend[i][j] + generalMarketTrend[i][j] + eventStream[i][j]));
				
				if (finalStockPrices[i][j] < stockPrices[i][j]) {
					finalStockPrices[i][j] = Double.parseDouble(decimalFormat.format(stockPrices[i][j]));
				}
			}
		}
	}
	
	
	//********************************************************************************************************
	//pass sector starting points as an argument
	//for example, for an evenly distributed 16 stocks by 4 sectors, the starting points of sectors would be
	//elements 0, 4, 8, 12 -- 0-3 -> first sector, 4-7 -> second sector and so on..
	//you can model the sector size as you want
	//********************************************************************************************************
	public static double[][] initialize(int[] sector) {
		sectorStartPositions = sector;
		generateStockPrices();
//		sectorStartPositions = new int[]{0, 4, 8, 12};
		
		generateRandomTrend();
		generateSectorTrend();
		generateGeneralMarketTrend();
		
		eventStream = MarketEvents.getEventStream(sectorStartPositions);
		addTrendsAndEvents();
		
		return finalStockPrices;
	}
	
	public static double[][] initialize(){
		return initialize(new int[] {0, 4, 8,12});
	}
}
