package com.blackcaps.constant;

public class TransactionType {
	public static final int CREDIT = 0;
	public static final int DEBIT = 1;
	
	public static final int SELL = 0;
	public static final int BUY = 1;
}
