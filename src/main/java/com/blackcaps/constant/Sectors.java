package com.blackcaps.constant;

public class Sectors {
	public static final String FINANCE = "FINANCE";
	public static final String CONSUMER = "CONSUMER";
	public static final String TECHNOLOGY = "TECHNOLOGY";
	public static final String MANUFACTURING = "MANUFACTURING";
}
