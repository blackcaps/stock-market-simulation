package com.blackcaps.constant;

public class Events {
	public static final String CRASH = "crash";
	public static final String BOOM = "boom";
	public static final String BUST = "bust";
	public static final String TAKEOVER = "takeover";
	public static final String SCANDAL = "scandal";
}
