package com.blackcaps.constant;

public class Stocks {
	public static final String HSBC = "HSBC";
	public static final String LOLC = "LOLC";
	public static final String SAMPATH = "SAMPATH";
	public static final String NSB = "NSB";
	
	public static final String BELLTACO = "BELLTACO";
	public static final String MCLOVIN = "MCLOVIN";
	public static final String KINGBURGER = "KINGBURGER";
	public static final String HUTPIZZA = "HUTPIZZA";
	
	public static final String Y98 = "98Y";
	public static final String MEGAHARD = "MEGAHARD";
	public static final String LABCAKES = "LABCAKES";
	public static final String KELLSJOHN = "KELLSJOHN";
	
	public static final String SONY = "SONY";
	public static final String VONOLE = "VONOLE";
	public static final String GATEWAY = "GATEWAY";
	public static final String HP = "HP";
	
	
	public static final Double STARTING_PRICE_MIN = 10.0;
	public static final Double STARTING_PRICE_MAX = 50.0;
	
	public static final int NO_OF_STOCKS_DEFAULT = 16;
}
