package com.blackcaps.constant;

public class Clock {
	public static final int RAPID_TURN_SECONDS = 5000;
	public static final int QUICK_TURN_SECONDS = 15000;
	public static final int SHORT_TURN_SECONDS = 30000;
	public static final int DEFAULT_TURN_SECONDS = 60000;
	public static final int LONG_TURN_SECONDS = 90000;
	
	public static final int NO_OF_TURNS_DEFAULT = 10;
}
