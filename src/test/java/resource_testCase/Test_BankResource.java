package resource_testCase;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.blackcaps.stock.resource.BankResource;
import com.blackcaps.stock.service.BankService;

public class Test_BankResource {

	private MockMvc mockMvc;
	
	@Mock private BankService bnkservice;
	
	@InjectMocks private BankResource bankRes;
	
	 @Before
    public void setUp() throws Exception {

	  mockMvc = MockMvcBuilders.standaloneSetup(bankRes).build();

	    }
	@Test
	public void test_BankResource() throws Exception{
		
		   mockMvc.perform(get("/balance/'"+1+"'")).andExpect(status().isOk()).andExpect(content().string("1"));
	 
	}

}
